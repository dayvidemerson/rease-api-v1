# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Importando do Python

import hashlib

from datetime import date

# Importando do Django

from django.contrib.auth.models import User
from django.test import TestCase

# Importando de Terceiros

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

# Importando do rease

from .models import *
from .serializers import *


class UserTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        admin = User(
            username='dayvidemerson96@gmail.com',
            email='dayvidemerson96@gmail.com',
            is_staff=True
        )
        admin.set_password('12345678')
        admin.save()
        self.admin = admin
        self.token = Token.objects.create(user=self.admin)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_email_reset_password(self):
        data = self.client.post(
            '/core/password/reset/',
            {
                'email': 'dayvidemerson96@gmail.com'
            },
            format='json'
        )
        self.assertEquals(200, data.status_code)
        self.assertEquals(
            'Verifique seu e-mail e clique no link do e-mail para redefinir sua senha.', data.data['message'])

        token = TokenResetPassword.objects.get(user=self.admin)
        data = self.client.put(
            '/core/password/reset/' + token.key + '/',
            {
                'password': '87654321',
                'confirm_password': '87654321'
            },
            format='json'
        )
        self.assertEquals(200, data.status_code)
        data = self.client.post(
            '/api-token-auth/',
            {
                'username': 'dayvidemerson96@gmail.com',
                'password': '87654321'
            },
            format='json'
        )
        self.assertEquals(200, data.status_code)
