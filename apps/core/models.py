from django.db import models
from django.contrib.auth.models import User

class Timestamp(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

class TokenResetPassword(Timestamp):
  user = models.ForeignKey(User, verbose_name='Usuário', on_delete=models.CASCADE)
  key = models.CharField(max_length=32, verbose_name='Chave')
  used = models.BooleanField(default=False, verbose_name='Usado')

  class Meta:
    verbose_name = 'Token de Recuperação de Senha'
    verbose_name_plural = 'Token\'s de Recuperação de Senha'

  def __str__(self):
    return self.key