from django.urls import include, path

from .views import *


app_name = 'core'

urlpatterns = [
	path('users/', UserView.as_view()),
	path('estados/', EstadoView.as_view()),
  path('password/reset/', send_password_reset_email, name='send-password-reset-email'),
  path('password/reset/<str:key>/', password_reset, name='password-reset'),
]