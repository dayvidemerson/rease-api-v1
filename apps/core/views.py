from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.response import Response
from rest_framework.views import APIView

from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import get_template

from .models import TokenResetPassword
from .serializers import PasswordResetSerializer
from .serializers import UserSerializer


class UserView(APIView):

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EstadoView(APIView):

    def get(self, request, format=None):
        return Response(STATE_CHOICES, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def send_password_reset_email(request):
    if request.method == 'POST' and 'email' in request.data:
        email = request.data['email']
        try:
            user = User.objects.get(email=email)
        except:
            error = {'error': 'E-mail não registrado em nosso sistema.'}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        try:
            token = TokenResetPassword.objects.get(user=user, used=False)
        except:
            token = TokenResetPassword.objects.create(user=user, used=False)
        ctx = {
            'url': 'https://participant.rease.com.br/password/reset/' + token.key
        }
        message = get_template(
            'email/password_reset.html').render(Context(ctx))
        msg = EmailMessage('Rease - Recuperação de Senha', message,
                           to=[email], from_email='contato@rease.com.br')
        msg.content_subtype = 'html'
        msg.send()
        success = {
            'message': 'Verifique seu e-mail e clique no link do e-mail para redefinir sua senha.'
        }
        return Response(success, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes((AllowAny,))
def password_reset(request, key):
    try:
        token = TokenResetPassword.objects.get(key=key, used=False)
    except:
        error = {
            'error': 'Token inválido. Por favor, requisite um novo token para recuperar sua senha.'}
        return Response(error, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'PUT':
        serializer = PasswordResetSerializer(token.user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            token.used = True
            token.save()
            success = {
                'message': 'Senha alterada com sucesso.'
            }
            return Response(success, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_400_BAD_REQUEST)
