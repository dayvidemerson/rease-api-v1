# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from hashlib import md5
from datetime import datetime

from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import TokenResetPassword

@receiver(pre_save, sender=TokenResetPassword)
def define_key_token_reset_password(sender, instance, *args, **kwargs):
  code = '%d - %s'%(instance.user.pk, datetime.now())
  instance.key = md5(code.encode()).hexdigest()