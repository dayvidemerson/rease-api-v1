from django.contrib.auth.models import User
from rest_framework import serializers


class PKSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)


class PasswordResetSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    confirm_password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['password', 'confirm_password']

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance

    def validate(self, data):
        if not data['password'] or not data['confirm_password']:
            raise serializers.ValidationError(
                "Por favor, entre com uma senha e a confirme.")

        if len(data['password']) < 8:
            raise serializers.ValidationError(
                'Por favor, insira uma senha com pelo menos 8 caracteres.')

        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError("As senhas não correspondem.")

        return data


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    password = serializers.CharField(write_only=True)
    confirm_password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        validated_data['username'] = validated_data['email']
        del validated_data['confirm_password']
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate_email(self, email):
        existing = User.objects.filter(email=email).first()
        if existing:
            raise serializers.ValidationError(
                "Esse e-mail já está registrado.")

        return email

    def validate(self, data):
        if not data['password'] or not data['confirm_password']:
            raise serializers.ValidationError(
                "Por favor, entre com uma senha e a confirme.")

        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError("As senhas não correspondem.")

        return data

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name',
                  'password', 'confirm_password']
