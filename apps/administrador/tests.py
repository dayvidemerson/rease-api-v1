# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date

from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token

from apps.evento.models import *
from .serializers import *


class AdministradorTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        admin = User(username='admin', is_staff=True)
        admin.set_password('12345678')
        admin.save()
        self.admin = admin
        self.token = Token.objects.create(user=self.admin)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.evento = Evento.objects.create(
            nome='TESTE', sigla='TST', administrador=admin)
        self.edicao = Edicao.objects.create(
            edicao='I',
            inicio=date(2016, 10, 10),
            termino=date(2016, 10, 10),
            inicio_inscricao=date(2016, 10, 10),
            termino_inscricao=date(2016, 10, 10),
            cep='64601-237',
            evento=self.evento
        )

        self.area = Area.objects.create(
            nome='Informática', administrador=admin)
        self.area1 = Area.objects.create(
            nome='Administração', administrador=admin)
        self.assinatura_diretor = AssinaturaDigital.objects.create(
            nome='Dayvid Emerson',
            cargo='Diretor Geral',
            imagem='dsadas',
            administrador=admin
        )

        self.assinatura_coordenador = AssinaturaDigital.objects.create(
            nome='Silva Ferreira',
            cargo='Coordenador de Pesquisa',
            imagem='djksakdsa',
            administrador=admin
        )

    def test_modelo_certificado(self):
        created = self.client.post(
            '/administrador/modelos/minicurso/',
            {
                'background': 'kldsad',
                'texto': 'Este é um certificado',
                'assinatura_diretor': self.assinatura_diretor.pk,
                'assinatura_coordenador': self.assinatura_coordenador.pk,
                'edicao': self.edicao.pk
            },
            format='json'
        )
        updated = self.client.put(
            '/administrador/modelos/minicurso/%s/' % (created.data['pk']),
            {
                'background': 'kldsad',
                'texto': 'Este é um certificado',
                'assinatura_diretor': self.assinatura_coordenador.pk,
                'assinatura_coordenador': self.assinatura_diretor.pk,
                'edicao': self.edicao.pk
            },
            format='json'
        )
        self.assertEquals(201, created.status_code)
        self.assertEquals(200, updated.status_code)

        created = self.client.post(
            '/administrador/modelos/atividade/',
            {
                'background': 'kldsad',
                'texto': 'Este é um certificado',
                'assinatura_diretor': self.assinatura_diretor.pk,
                'assinatura_coordenador': self.assinatura_coordenador.pk,
                'edicao': self.edicao.pk
            },
            format='json'
        )
        updated = self.client.put(
            '/administrador/modelos/atividade/%s/' % (created.data['pk']),
            {
                'background': 'kldsad',
                'texto': 'Este é um certificado',
                'assinatura_diretor': self.assinatura_coordenador.pk,
                'assinatura_coordenador': self.assinatura_diretor.pk,
                'edicao': self.edicao.pk
            },
            format='json'
        )
        self.assertEquals(201, created.status_code)
        self.assertEquals(200, updated.status_code)

        created = self.client.post(
            '/administrador/modelos/palestra/',
            {
                'background': 'kldsad',
                'texto': 'Este é um certificado',
                'assinatura_diretor': self.assinatura_diretor.pk,
                'assinatura_coordenador': self.assinatura_coordenador.pk,
                'edicao': self.edicao.pk
            },
            format='json'
        )
        updated = self.client.put(
            '/administrador/modelos/palestra/%s/' % (created.data['pk']),
            {
                'background': 'kldsad',
                'texto': 'Este é um certificado',
                'assinatura_diretor': self.assinatura_coordenador.pk,
                'assinatura_coordenador': self.assinatura_diretor.pk,
                'edicao': self.edicao.pk
            },
            format='json'
        )
        self.assertEquals(201, created.status_code)
        self.assertEquals(200, updated.status_code)

    def test_edicao_finalizada(self):
        created = self.client.post(
            '/administrador/edicoes/',
            {
                'edicao': 'I',
                'inicio': '2000-12-31',
                'termino': '2001-01-31',
                'inicio_inscricao': '2000-12-31',
                'termino_inscricao': '2001-01-31',
                'cep': '64601-237',
                'areas': [self.area.pk, self.area1.pk],
                'evento': self.evento.pk
            },
            format='json'
        )
        updated = self.client.put(
            '/administrador/edicoes/%s/' % (created.data['pk']),
            {
                'edicao': 'I',
                'inicio': '2000-12-31',
                'termino': '2001-01-31',
                'inicio_inscricao': '2000-12-31',
                'termino_inscricao': '2001-01-31',
                'cep': '64601-237',
                'areas': [self.area.pk],
                'evento': self.evento.pk
            },
            format='json'
        )
        self.assertEquals(201, created.status_code)
        self.assertEquals(200, updated.status_code)
