# -*- coding: utf-8 -*-
from base64 import b64decode
from rest_framework import serializers

from apps.core.serializers import PKSerializer
from apps.evento.models import *
from rease.settings import MEDIA_ROOT
from rease.settings import MEDIA_URL


class EventoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Evento
        exclude = ['administrador']


class AreaSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Area
        exclude = ['administrador']


class AssinaturaDigitalSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = AssinaturaDigital
        fields = '__all__'

    def validate_imagem(self, data):
        if 'http' not in data:
            try:
                last = AssinaturaDigital.objects.latest('pk').pk
            except:
                last = 0

            value = data
            data = '%s/assinatura/assinatura-%s.png' % (
                MEDIA_URL, last)

            if 'base64,' in value:
                value = value.split('base64,')[1]

            path = '%s/assinatura/assinatura-%s.png' % (MEDIA_ROOT, last)
            fh = open(path, 'wb')
            fh.write(b64decode(value))
            fh.close()
        return data


class EnderecoSerializer(serializers.Serializer):
    cep = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    logradouro = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    numero = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    bairro = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    cidade = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    estado = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    complemento = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class EdicaoSerializer(PKSerializer, EnderecoSerializer, serializers.ModelSerializer):
    nome = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = Edicao
        fields = '__all__'

    def get_nome(self, obj):
        return obj.nome()

    def get_status(self, obj):
        return obj.status()

    def validate(self, data):
        if 'logomarca' in data and 'http' not in data['logomarca']:
            try:
                last = Edicao.objects.latest('pk').pk
            except:
                last = 0

            value = data['logomarca']
            data['logomarca'] = '%s/logomarca/logomarca-%s.png' % (
                MEDIA_URL, last)

            if 'base64,' in value:
                value = value.split('base64,')[1]

            path = '%s/logomarca/logomarca-%s.png' % (MEDIA_ROOT, last)
            fh = open(path, 'wb')
            fh.write(b64decode(value))
            fh.close()
        return data


class ConfiguracaoCertificadoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = ConfiguracaoCertificado
        fields = '__all__'


class ModeloCertificadoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = ModeloCertificado
        fields = '__all__'

    def validate_background_frente(self, data):
        if 'http' not in data:
            try:
                last = ModeloCertificado.objects.latest('pk').pk
            except:
                last = 0

            value = data
            data = '%s/background_frente/background-%s.png' % (
                MEDIA_URL, last)

            if 'base64,' in value:
                value = value.split('base64,')[1]

            path = '%s/background_frente/background-%s.png' % (
                MEDIA_ROOT, last)
            fh = open(path, 'wb')
            fh.write(b64decode(value))
            fh.close()
        return data

    def validate_background_costa(self, data):
        if 'http' not in data:
            try:
                last = ModeloCertificado.objects.latest('pk').pk
            except:
                last = 0

            value = data
            data = '%s/background_costa/background-%s.png' % (
                MEDIA_URL, last)

            if 'base64,' in value:
                value = value.split('base64,')[1]

            path = '%s/background_costa/background-%s.png' % (
                MEDIA_ROOT, last)
            fh = open(path, 'wb')
            fh.write(b64decode(value))
            fh.close()
        return data


class ModeloCertificadoMinicursoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = ModeloCertificadoMinicurso
        fields = '__all__'


class ModeloCertificadoPalestraSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = ModeloCertificadoPalestra
        fields = '__all__'


class ModeloCertificadoAtividadeSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = ModeloCertificadoAtividade
        fields = '__all__'


class ModeloCertificadoEventoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = ModeloCertificadoEvento
        fields = '__all__'


class ProfissionalSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Profissional
        exclude = ['administrador']


class AtividadeGeralSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = AtividadeGeral
        fields = '__all__'


class ParticipacaoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Participacao
        fields = '__all__'


class MinicursoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Minicurso
        fields = '__all__'


class PalestraSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Palestra
        fields = '__all__'


class AtividadeSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Atividade
        fields = '__all__'


class GrupoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = Grupo
        fields = '__all__'


class TurmaMinicursoSerializer(PKSerializer, serializers.ModelSerializer):
    vagas_disponiveis = serializers.SerializerMethodField()

    class Meta:
        model = TurmaMinicurso
        fields = '__all__'

    def get_vagas_disponiveis(self, obj):
        return obj.vagas_disponiveis()


class TurmaAtividadeSerializer(PKSerializer, serializers.ModelSerializer):
    vagas_disponiveis = serializers.SerializerMethodField()

    class Meta:
        model = TurmaAtividade
        fields = '__all__'

    def get_vagas_disponiveis(self, obj):
        return obj.vagas_disponiveis()


class HorarioMinicursoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = HorarioMinicurso
        fields = '__all__'


class HorarioAtividadeSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = HorarioAtividade
        fields = '__all__'


class TipoInscricaoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = TipoInscricao
        fields = '__all__'


class TipoDocumentoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = TipoDocumento
        fields = '__all__'


class InscricaoSerializer(PKSerializer, serializers.ModelSerializer):
    participante = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    tipo = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    documentos = serializers.SerializerMethodField()
    participacoes = serializers.SerializerMethodField()
    certificado_evento = serializers.SerializerMethodField()
    certificado_disponivel = serializers.SerializerMethodField()
    minicursos = serializers.SerializerMethodField()
    palestras = serializers.SerializerMethodField()
    atividades = serializers.SerializerMethodField()

    class Meta:
        model = Inscricao
        fields = '__all__'
        read_only_fields = ['pk', 'preco']

    def get_participante(self, obj):
        return obj.participante.get_full_name()

    def get_email(self, obj):
        return obj.participante.email

    def get_tipo(self, obj):
        return obj.tipo.nome

    def get_status(self, obj):
        return obj.get_status_display()

    def get_documentos(self, obj):
        documentos = obj.inscricao_documento.all()
        serializer = DocumentoSerializer(documentos, many=True)
        return serializer.data

    def get_participacoes(self, obj):
        participacoes = obj.inscricao_participacao.all()
        serializer = ParticipacaoSerializer(participacoes, many=True)
        return serializer.data

    def get_certificado_evento(self, obj):
        certificado = None
        if hasattr(obj, 'inscricao_certificadoevento'):
            certificado = obj.inscricao_certificadoevento.pk
        return certificado

    def get_certificado_disponivel(self, obj):
        certificado = False
        if hasattr(obj, 'inscricao_certificadoevento'):
            certificado = obj.inscricao_certificadoevento.disponivel
        return certificado

    def get_minicursos(self, obj):
        certificados = obj.inscricao_certificadominicurso.all()
        turmas = []
        for certificado in certificados:
            turmas.append(certificado.turma)
        serializer = TurmaMinicursoSerializer(turmas, many=True)
        return serializer.data

    def get_atividades(self, obj):
        certificados = obj.inscricao_certificadoatividade.all()
        turmas = []
        for certificado in certificados:
            turmas.append(certificado.turma)
        serializer = TurmaAtividadeSerializer(turmas, many=True)
        return serializer.data

    def get_palestras(self, obj):
        palestras = Palestra.objects.filter(edicao=obj.tipo.edicao)
        serializer = PalestraSerializer(palestras, many=True)
        return serializer.data


class DocumentoSerializer(PKSerializer, serializers.ModelSerializer):
    tipo = serializers.SerializerMethodField()

    class Meta:
        model = Documento
        fields = '__all__'

    def get_tipo(self, obj):
        return obj.tipo.nome


class CertificadoMinicursoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = CertificadoMinicurso
        fields = '__all__'


class CertificadoPalestraSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = CertificadoPalestra
        fields = '__all__'


class CertificadoAtividadeSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = CertificadoAtividade
        fields = '__all__'


class CertificadoEventoSerializer(PKSerializer, serializers.ModelSerializer):

    class Meta:
        model = CertificadoEvento
        fields = '__all__'


class EdicaoRelatorioSerializer(PKSerializer, serializers.ModelSerializer):
    inscricoes = serializers.SerializerMethodField()
    confirmadas = serializers.SerializerMethodField()
    pendentes = serializers.SerializerMethodField()
    palestras = serializers.SerializerMethodField()
    minicursos = serializers.SerializerMethodField()
    vagas = serializers.SerializerMethodField()

    class Meta:
        model = Edicao
        fields = ['inscricoes', 'confirmadas', 'pendentes',
                  'palestras', 'minicursos', 'vagas']

    def get_inscricoes(self, obj):
        tipos = obj.edicao_tipoinscricao.all()
        quantidade = 0
        for tipo in tipos:
            quantidade += tipo.tipo_inscricao.count()
        return quantidade

    def get_confirmadas(self, obj):
        tipos = obj.edicao_tipoinscricao.all()
        quantidade = 0
        for tipo in tipos:
            quantidade += tipo.tipo_inscricao.filter(status='C').count()
        return quantidade

    def get_pendentes(self, obj):
        tipos = obj.edicao_tipoinscricao.all()
        quantidade = 0
        for tipo in tipos:
            quantidade += tipo.tipo_inscricao.filter(status='I').count()
        return quantidade

    def get_palestras(self, obj):
        palestras = obj.edicao_palestra.all()
        quantidade = 0
        for palestra in palestras:
            quantidade += palestra.palestra_certificadopalestra.count()
        return quantidade

    def get_minicursos(self, obj):
        grupos = obj.edicao_grupo.all()
        quantidade = 0
        for grupo in grupos:
            turmas = grupo.grupo_turmaminicurso.all()
            for turma in turmas:
                quantidade += turma.turma_certificadominicurso.count()
        return quantidade

    def get_vagas(self, obj):
        grupos = obj.edicao_grupo.all()
        quantidade = 0
        for grupo in grupos:
            turmas = grupo.grupo_turmaminicurso.all()
            for turma in turmas:
                quantidade += turma.vagas_disponiveis()
        return quantidade


class MinicursoRelatorioSerializer(PKSerializer, serializers.ModelSerializer):
    nome = serializers.SerializerMethodField()
    grupo = serializers.SerializerMethodField()
    vagas_disponiveis = serializers.SerializerMethodField()
    participantes = serializers.SerializerMethodField()

    class Meta:
        model = TurmaMinicurso
        fields = '__all__'
        read_only_fields = ['pk', 'vagas']

    def get_nome(self, obj):
        return obj.minicurso.nome

    def get_grupo(self, obj):
        return obj.grupo.nome

    def get_vagas_disponiveis(self, obj):
        return obj.vagas_disponiveis()

    def get_participantes(self, obj):
        certificados = obj.turma_certificadominicurso.all().order_by(
            'inscricao__participante__first_name')
        participantes = []
        for certificado in certificados:
            participantes.append({
                'nome': certificado.inscricao.participante.get_full_name(),
                'email': certificado.inscricao.participante.email,
                'certificado': certificado.pk,
                'disponivel': certificado.disponivel
            })
        return participantes


class AtividadeRelatorioSerializer(PKSerializer, serializers.ModelSerializer):
    nome = serializers.SerializerMethodField()
    grupo = serializers.SerializerMethodField()
    vagas_disponiveis = serializers.SerializerMethodField()
    participantes = serializers.SerializerMethodField()

    class Meta:
        model = TurmaAtividade
        fields = '__all__'
        read_only_fields = ['pk', 'vagas']

    def get_nome(self, obj):
        return obj.atividade.nome

    def get_grupo(self, obj):
        return obj.grupo.nome

    def get_vagas_disponiveis(self, obj):
        return obj.vagas_disponiveis()

    def get_participantes(self, obj):
        certificados = obj.turma_certificadoatividade.all().order_by(
            'inscricao__participante__first_name')
        participantes = []
        for certificado in certificados:
            participantes.append({
                'nome': certificado.inscricao.participante.get_full_name(),
                'certificado': certificado.pk,
                'disponivel': certificado.disponivel
            })
        return participantes


class PalestraRelatorioSerializer(PKSerializer, serializers.ModelSerializer):
    nome = serializers.SerializerMethodField()
    participantes = serializers.SerializerMethodField()

    class Meta:
        model = Palestra
        fields = '__all__'

    def get_nome(self, obj):
        return obj.nome

    def get_participantes(self, obj):
        certificados = obj.palestra_certificadopalestra.all(
        ).order_by('inscricao__participante__first_name')
        participantes = []
        for certificado in certificados:
            participantes.append({
                'nome': certificado.inscricao.participante.get_full_name(),
                'certificado': certificado.pk,
                'disponivel': certificado.disponivel
            })
        return participantes
