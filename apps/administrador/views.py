# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from base64 import b64encode

from django.template import Context
from django.template.loader import get_template

from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import api_view, permission_classes
from pydf import generate_pdf

from apps.core.permissions import IsStaff
from apps.evento.models import *
from .serializers import *


def last_certificado(edicao):
    lista = []
    certificado = CertificadoMinicurso.objects.filter(
        numero__isnull=False, turma__minicurso__edicao=edicao
    ).values_list(
        'numero'
    ).order_by(
        'numero'
    ).last()

    if certificado:
        lista.append(certificado)
    certificado = CertificadoAtividade.objects.filter(
        numero__isnull=False, turma__atividade__edicao=edicao
    ).values_list(
        'numero'
    ).order_by(
        'numero'
    ).last()

    if certificado:
        lista.append(certificado)
    certificado = CertificadoPalestra.objects.filter(
        numero__isnull=False, palestra__edicao=edicao
    ).values_list(
        'numero'
    ).order_by(
        'numero'
    ).last()

    if certificado:
        lista.append(certificado)
    certificado = CertificadoEvento.objects.filter(
        numero__isnull=False, inscricao__tipo__edicao=edicao
    ).values_list(
        'numero'
    ).order_by(
        'numero'
    ).last()

    if certificado:
        lista.append(certificado)

    if lista:
        return max(lista)[0]
    return None


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def generate_lista_palestra(request, pk):
    try:
        palestra = Palestra.objects.get(pk=pk)
    except:
        return Response(
            'Palestra não existe.', status=status.HTTP_400_BAD_REQUEST)
    pks_participante = palestra.palestra_certificadopalestra.values_list(
        'inscricao__participante__pk')
    participantes = Participante.objects.filter(
        pk__in=pks_participante).order_by('first_name')
    template = get_template('lista_presenca.html')
    context = Context({
        'evento': palestra.edicao.nome(),
        'atividade': palestra.nome,
        'participantes': participantes
    })
    html = template.render(context)
    pdf = generate_pdf(html, margin_bottom='10', margin_left='10',
                       margin_right='10', margin_top='20', page_size='A4')
    pdf_base64 = b64encode(pdf)
    return Response(pdf_base64, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def generate_lista_minicurso(request, pk):
    try:
        turma = TurmaMinicurso.objects.get(pk=pk)
    except:
        return Response(
            'Minicurso não existe.', status=status.HTTP_400_BAD_REQUEST)
    pks_participante = turma.turma_certificadominicurso.values_list(
        'inscricao__participante__pk')
    participantes = Participante.objects.filter(
        pk__in=pks_participante).order_by('first_name')
    template = get_template('lista_presenca.html')
    context = Context({
        'evento': turma.minicurso.edicao.nome(),
        'atividade': turma.minicurso.nome,
        'participantes': participantes
    })
    html = template.render(context)
    pdf = generate_pdf(html, margin_bottom='10', margin_left='10',
                       margin_right='10', margin_top='20', page_size='A4')
    pdf_base64 = b64encode(pdf)
    return Response(pdf_base64, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def generate_lista_atividade(request, pk):
    try:
        turma = TurmaAtividade.objects.get(pk=pk)
    except:
        return Response(
            'Atividade não existe.', status=status.HTTP_400_BAD_REQUEST)

    pks_participante = turma.turma_certificadoatividade.values_list(
        'inscricao__participante__pk')
    participantes = Participante.objects.filter(
        pk__in=pks_participante).order_by('first_name')
    template = get_template('lista_presenca_atividade.html')
    context = Context({
        'evento': turma.atividade.edicao.nome(),
        'atividade': turma.atividade.nome,
        'participantes': participantes
    })
    html = template.render(context)
    pdf = generate_pdf(html, margin_bottom='10', margin_left='10',
                       margin_right='10', margin_top='20', page_size='A4')
    pdf_base64 = b64encode(pdf)
    return Response(pdf_base64, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def generate_lista_atividade_geral(request, pk):
    try:
        atividade = AtividadeGeral.objects.get(pk=pk)
    except:
        return Response(
            'Atividade não existe.', status=status.HTTP_400_BAD_REQUEST)

    pks_participante = Inscricao.objects.filter(
        tipo__edicao=atividade.edicao).values_list('participante')
    participantes = Participante.objects.filter(
        pk__in=pks_participante).order_by('first_name', 'last_name')
    template = get_template('lista_presenca_atividade.html')
    context = Context({
        'evento': atividade.edicao.nome(),
        'atividade': atividade.descricao,
        'participantes': participantes
    })
    html = template.render(context)
    pdf = generate_pdf(html, margin_bottom='10', margin_left='10',
                       margin_right='10', margin_top='20', page_size='A4')
    pdf_base64 = b64encode(pdf)
    return Response(pdf_base64, status=status.HTTP_200_OK)


@api_view(['PUT'])
@permission_classes((IsAuthenticated, IsStaff))
def available_certificado(request, pk):
    if request.method == 'PUT':
        try:
            certificado = Certificado.objects.get(pk=pk)
        except Exception as e:
            return Response(
                'Certificado não existe', status=status.HTTP_400_BAD_REQUEST)

        certificado.disponivel = request.data['disponivel']

        if certificado.disponivel and not certificado.numero:
            edicao = None
            evento = CertificadoEvento.objects.filter(
                pk=pk
            ).first()

            if evento:
                certificado.nome_certificado = evento.inscricao.participante.get_full_name()
                edicao = evento.inscricao.tipo.edicao

            if not edicao:
                minicurso = CertificadoMinicurso.objects.filter(
                    pk=pk
                ).first()
                if minicurso:
                    edicao = minicurso.turma.minicurso.edicao

            if not edicao:
                palestra = CertificadoPalestra.objects.filter(
                    pk=pk
                ).first()
                if palestra:
                    edicao = palestra.palestra.edicao

            if not edicao:
                atividade = CertificadoAtividade.objects.filter(
                    pk=pk
                ).first()
                if atividade:
                    edicao = atividade.turma.atividade.edicao

            if hasattr(edicao, 'edicao_configuracaocertificado'):
                last = last_certificado(edicao)
                if last:
                    certificado.numero = last + 1
                else:
                    configuracao = edicao.edicao_configuracaocertificado
                    certificado.numero = configuracao.numero_inicio_certificado

        certificado.save()
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes((IsAuthenticated, IsStaff))
def activate_inscricao(request, pk):
    if request.method == 'PUT':
        inscricao = Inscricao.objects.get(pk=pk)
        if not inscricao.ignore_pending:
            inscricao.ignore_pending = True
        if request.data['active']:
            inscricao.status = 'C'
        else:
            inscricao.status = 'I'

        inscricao.save()
        serializer = InscricaoSerializer(inscricao)
        return Response(serializer.data)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes((IsAuthenticated, IsStaff))
def validate_documento(request, pk):
    if request.method == 'PUT':
        documento = Documento.objects.get(pk=pk)
        documento.valido = request.data['validate']
        documento.save()
        serializer = DocumentoSerializer(documento)
        return Response(serializer.data)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def edicao_relatorio(request, pk):
    if request.method == 'GET':
        try:
            edicao = Edicao.objects.get(pk=pk)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = EdicaoRelatorioSerializer(edicao)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def minicurso_relatorio(request, pk):
    if request.method == 'GET':
        try:
            minicursos = TurmaMinicurso.objects.filter(grupo__edicao=pk)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = MinicursoRelatorioSerializer(minicursos, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def atividade_relatorio(request, pk):
    if request.method == 'GET':
        try:
            atividades = TurmaAtividade.objects.filter(grupo__edicao=pk)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = AtividadeRelatorioSerializer(atividades, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsStaff))
def palestra_relatorio(request, pk):
    if request.method == 'GET':
        try:
            palestras = Palestra.objects.filter(edicao=pk)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = PalestraRelatorioSerializer(palestras, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


class InscricaoListView(ListAPIView):
    queryset = Inscricao.objects.none()
    serializer_class = InscricaoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['tipo__edicao']

    def get_queryset(self):
        return Inscricao.objects.filter(
            tipo__edicao__evento__administrador=self.request.user
        ).order_by(
            'participante__first_name')


class DocumentoListView(ListAPIView):
    queryset = Documento.objects.none()
    serializer_class = DocumentoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['inscricao']

    def get_queryset(self):
        user = self.request.user
        return Documento.objects.filter(
            tipo__tipo_inscricao__edicao__evento__administrador=user)


class EventoViewSet(viewsets.ModelViewSet):
    queryset = Evento.objects.none()
    serializer_class = EventoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]

    def get_queryset(self):
        return Evento.objects.filter(administrador=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(administrador=self.request.user)


class AreaViewSet(viewsets.ModelViewSet):
    queryset = Area.objects.none()
    serializer_class = AreaSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]

    def get_queryset(self):
        return Area.objects.filter(administrador=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(administrador=self.request.user)


class EdicaoViewSet(viewsets.ModelViewSet):
    queryset = Edicao.objects.none()
    serializer_class = EdicaoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['evento']

    def get_queryset(self):
        return Edicao.objects.filter(evento__administrador=self.request.user)


class ConfiguracaoCertificadoViewSet(viewsets.ModelViewSet):
    queryset = ConfiguracaoCertificado.objects.none()
    serializer_class = ConfiguracaoCertificadoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return ConfiguracaoCertificado.objects.filter(
            edicao__evento__administrador=self.request.user)


class ModeloCertificadoMinicursoViewSet(viewsets.ModelViewSet):
    queryset = ModeloCertificadoMinicurso.objects.none()
    serializer_class = ModeloCertificadoMinicursoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return ModeloCertificadoMinicurso.objects.filter(
            edicao__evento__administrador=self.request.user)


class ModeloCertificadoPalestraViewSet(viewsets.ModelViewSet):
    queryset = ModeloCertificadoPalestra.objects.none()
    serializer_class = ModeloCertificadoPalestraSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return ModeloCertificadoPalestra.objects.filter(
            edicao__evento__administrador=self.request.user)


class ModeloCertificadoAtividadeViewSet(viewsets.ModelViewSet):
    queryset = ModeloCertificadoAtividade.objects.none()
    serializer_class = ModeloCertificadoAtividadeSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return ModeloCertificadoAtividade.objects.filter(
            edicao__evento__administrador=self.request.user)


class ModeloCertificadoEventoViewSet(viewsets.ModelViewSet):
    queryset = ModeloCertificadoEvento.objects.none()
    serializer_class = ModeloCertificadoEventoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return ModeloCertificadoEvento.objects.filter(
            edicao__evento__administrador=self.request.user)


class ProfissionalViewSet(viewsets.ModelViewSet):
    queryset = Profissional.objects.all()
    serializer_class = ProfissionalSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]

    def get_queryset(self):
        return Profissional.objects.filter(administrador=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(administrador=self.request.user)


class AssinaturaDigitalViewSet(viewsets.ModelViewSet):
    queryset = AssinaturaDigital.objects.all()
    serializer_class = AssinaturaDigitalSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]

    def get_queryset(self):
        return AssinaturaDigital.objects.filter(
            administrador=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(administrador=self.request.user)


class AtividadeGeralViewSet(viewsets.ModelViewSet):
    queryset = AtividadeGeral.objects.none()
    serializer_class = AtividadeGeralSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return AtividadeGeral.objects.filter(
            edicao__evento__administrador=self.request.user)


class ParticipacaoViewSet(viewsets.ModelViewSet):
    queryset = Participacao.objects.none()
    serializer_class = ParticipacaoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['atividade_geral']

    def get_queryset(self):
        return Participacao.objects.filter(
            atividade_geral__edicao__evento__administrador=self.request.user)


class MinicursoViewSet(viewsets.ModelViewSet):
    queryset = Minicurso.objects.none()
    serializer_class = MinicursoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return Minicurso.objects.filter(
            edicao__evento__administrador=self.request.user)


class TurmaMinicursoViewSet(viewsets.ModelViewSet):
    queryset = TurmaMinicurso.objects.none()
    serializer_class = TurmaMinicursoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['minicurso']

    def get_queryset(self):
        return TurmaMinicurso.objects.filter(
            minicurso__edicao__evento__administrador=self.request.user)


class HorarioMinicursoViewSet(viewsets.ModelViewSet):
    queryset = HorarioMinicurso.objects.none()
    serializer_class = HorarioMinicursoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['turma']

    def get_queryset(self):
        return HorarioMinicurso.objects.filter(
            turma__minicurso__edicao__evento__administrador=self.request.user)


class PalestraViewSet(viewsets.ModelViewSet):
    queryset = Palestra.objects.none()
    serializer_class = PalestraSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return Palestra.objects.filter(
            edicao__evento__administrador=self.request.user)


class AtividadeViewSet(viewsets.ModelViewSet):
    queryset = Atividade.objects.none()
    serializer_class = AtividadeSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return Atividade.objects.filter(
            edicao__evento__administrador=self.request.user)


class TurmaAtividadeViewSet(viewsets.ModelViewSet):
    queryset = TurmaAtividade.objects.none()
    serializer_class = TurmaAtividadeSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['atividade']

    def get_queryset(self):
        return TurmaAtividade.objects.filter(
            atividade__edicao__evento__administrador=self.request.user)


class HorarioAtividadeViewSet(viewsets.ModelViewSet):
    queryset = HorarioAtividade.objects.none()
    serializer_class = HorarioAtividadeSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['turma']

    def get_queryset(self):
        return HorarioAtividade.objects.filter(
            turma__atividade__edicao__evento__administrador=self.request.user)


class GrupoViewSet(viewsets.ModelViewSet):
    queryset = Grupo.objects.none()
    serializer_class = GrupoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return Grupo.objects.filter(
            edicao__evento__administrador=self.request.user)


class TipoInscricaoViewSet(viewsets.ModelViewSet):
    queryset = TipoInscricao.objects.none()
    serializer_class = TipoInscricaoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']

    def get_queryset(self):
        return TipoInscricao.objects.filter(
            edicao__evento__administrador=self.request.user)


class TipoDocumentoViewSet(viewsets.ModelViewSet):
    queryset = TipoDocumento.objects.none()
    serializer_class = TipoDocumentoSerializer
    permission_classes = [IsAuthenticated, IsStaff]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['tipo_inscricao']

    def get_queryset(self):
        return TipoDocumento.objects.filter(
            tipo_inscricao__edicao__evento__administrador=self.request.user)
