from django.urls import path
from rest_framework.routers import DefaultRouter
from . import views

app_name = 'administrador'

router = DefaultRouter()
router.register(r'eventos', views.EventoViewSet)
router.register(r'areas', views.AreaViewSet)
router.register(r'edicoes', views.EdicaoViewSet)
router.register(r'assinaturas', views.AssinaturaDigitalViewSet)
router.register(r'profissionais', views.ProfissionalViewSet)
router.register(r'assinaturas', views.AssinaturaDigitalViewSet)
router.register(r'minicursos', views.MinicursoViewSet)
router.register(r'atividades/geral', views.AtividadeGeralViewSet)
router.register(r'participacao', views.ParticipacaoViewSet)
router.register(r'palestras', views.PalestraViewSet)
router.register(r'atividades', views.AtividadeViewSet)
router.register(r'grupos', views.GrupoViewSet)
router.register(
    r'configuracoes/certificado', views.ConfiguracaoCertificadoViewSet)
router.register(r'modelos/minicurso', views.ModeloCertificadoMinicursoViewSet)
router.register(r'modelos/palestra', views.ModeloCertificadoPalestraViewSet)
router.register(r'modelos/atividade', views.ModeloCertificadoAtividadeViewSet)
router.register(r'turmas/minicurso', views.TurmaMinicursoViewSet)
router.register(r'turmas/atividade', views.TurmaAtividadeViewSet)
router.register(r'horarios/minicurso', views.HorarioMinicursoViewSet)
router.register(r'horarios/atividade', views.HorarioAtividadeViewSet)
router.register(r'tipos/inscricao', views.TipoInscricaoViewSet)
router.register(r'tipos/documento', views.TipoDocumentoViewSet)

urlpatterns = [
    path('inscricoes/',
        views.InscricaoListView.as_view(), name='inscricao-list-api'),
    path('inscricoes/<int:pk>/',
        views.activate_inscricao, name='inscricao-activate'),
    path('edicoes/relatorio/<int:pk>/',
        views.edicao_relatorio, name='edicao-relatorio'),
    path('minicursos/relatorio/<int:pk>/',
        views.minicurso_relatorio, name='minicurso-relatorio'),
    path('atividades/relatorio/<int:pk>/',
        views.atividade_relatorio, name='atividade-relatorio'),
    path('palestras/relatorio/<int:pk>/',
        views.palestra_relatorio, name='palestra-relatorio'),
    path('certificados/<int:pk>/',
        views.available_certificado, name='available-certificado'),
    path('documentos/',
        views.DocumentoListView.as_view(), name='documento-list-api'),
    path('documentos/<int:pk>/',
        views.validate_documento, name='documento-validate'),
    path('participantes/palestra/<int:pk>/pdf/',
        views.generate_lista_palestra, name='lista-presenca-palestra'),
    path('participantes/minicurso/<int:pk>/pdf/',
        views.generate_lista_minicurso, name='lista-presenca-minicurso'),
    path('participantes/atividade/<int:pk>/pdf/',
        views.generate_lista_atividade, name='lista-presenca-atividade'),
    path('participantes/atividade_geral/<int:pk>/pdf/',
        views.generate_lista_atividade_geral, name='lista-presenca-atividade-geral'),
]

urlpatterns += router.urls
