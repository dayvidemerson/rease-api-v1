from rest_framework.permissions import BasePermission
from .models import Participante


class IsParticipante(BasePermission):

    def has_permission(self, request, view):
        try:
            participante = Participante.objects.get(pk=request.user.pk)
        except:
            return False
        return True
