# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from hashlib import md5
from datetime import date
from random import randint

from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from .models import Inscricao, Documento, CertificadoMinicurso, \
    CertificadoPalestra, CertificadoEvento, CertificadoAtividade


def get_status_inscricao(instance):
    if not instance.ignore_pending:
        if instance.documentos_necessarios():
            return 'I'
        else:
            return 'C'
    return instance.status


@receiver(pre_save, sender=Inscricao)
def define_status_inscricao(sender, instance, *args, **kwargs):
    instance.status = get_status_inscricao(instance)


@receiver(post_save, sender=Documento)
def define_status_inscricao_in_documento(sender, instance, *args, **kwargs):
    instance = instance.inscricao
    instance.status = get_status_inscricao(instance)
    instance.save()


@receiver(pre_save, sender=CertificadoEvento)
@receiver(pre_save, sender=CertificadoMinicurso)
@receiver(pre_save, sender=CertificadoPalestra)
@receiver(pre_save, sender=CertificadoAtividade)
def generate_token_certificado(sender, instance, *args, **kwargs):
    edicao = instance.inscricao.tipo.edicao
    instance.data_emissao = edicao.termino
    code = ''
    if isinstance(instance, CertificadoMinicurso):
        code = 'Inscrição %s Minicurso %s' % (
            instance.inscricao.pk, instance.turma.pk)
    elif isinstance(instance, CertificadoPalestra):
        code = 'Inscrição %s Palestra %s' % (
            instance.inscricao.pk, instance.palestra.pk)
    elif isinstance(instance, CertificadoEvento):
        code = 'Inscrição %s Evento %s' % (
            instance.inscricao.pk, instance.inscricao.tipo.edicao.pk)
    elif isinstance(instance, CertificadoAtividade):
        code = 'Inscrição %s Atividade %s' % (
            instance.inscricao.pk, instance.turma.pk)

    instance.token = md5(code.encode()).hexdigest()


@receiver(post_save, sender=CertificadoMinicurso)
@receiver(post_save, sender=CertificadoPalestra)
@receiver(post_save, sender=CertificadoAtividade)
def generate_preco_inscricao(sender, instance, *args, **kwargs):
    instance = instance.inscricao
    instance.preco = 0
    minicursos = instance.inscricao_certificadominicurso.count()
    palestras = instance.inscricao_certificadopalestra.count()
    atividades = instance.inscricao_certificadoatividade.count()
    if (instance.tipo.data_aumento and
            instance.tipo.data_aumento <= date.today()):
        if (minicursos and
                instance.tipo.preco_minicurso_aumento):
            preco = minicursos * instance.tipo.preco_minicurso_aumento
            instance.preco += preco
        if (palestras and
                instance.tipo.preco_palestra_aumento):
            instance.preco += instance.tipo.preco_palestra_aumento
        if (atividades and
                instance.tipo.preco_atividade_aumento):
            instance.preco += instance.tipo.preco_atividade_aumento
    else:
        if (minicursos and
                instance.tipo.preco_minicurso):
            instance.preco += minicursos * instance.tipo.preco_minicurso

        if (palestras and
                instance.tipo.preco_palestra):
            instance.preco += instance.tipo.preco_palestra

        if (atividades and
                instance.tipo.preco_atividade):
            instance.preco += atividades * instance.tipo.preco_atividade

    instance.save()
