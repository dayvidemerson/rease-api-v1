from django.apps import AppConfig


class EventoConfig(AppConfig):
  name = 'apps.evento'

  def ready(self):
    from . import signals