# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication

from .serializers import ParticipanteSerializer, ParticipanteUpdateSerializer
from .models import Participante


class ParticipanteView(APIView):
    permission_classes = []
    authentication_classes = [TokenAuthentication]

    def get(self, request, format=None):
        participante = Participante.objects.get(pk=request.user.pk)
        serializer = ParticipanteUpdateSerializer(participante)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ParticipanteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, format=None):
        participante = Participante.objects.get(pk=request.user.pk)
        serializer = ParticipanteUpdateSerializer(
            participante, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
