from django.urls import path
from .views import ParticipanteView


app_name = 'evento'

urlpatterns = [
    path('participante/', ParticipanteView.as_view(), name='participante-api'),
]
