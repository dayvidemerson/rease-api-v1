# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date

from django.contrib.auth.models import User
from django.db import models

from apps.core.models import Timestamp


class Participante(User, Timestamp):
    data_nascimento = models.DateField(default='2000-01-01')
    cpf = models.CharField(max_length=14)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.email


class Evento(Timestamp):
    nome = models.CharField(max_length=100)
    sigla = models.CharField(max_length=20)
    administrador = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='administrador_evento')

    def __str__(self):
        return self.nome


class Area(Timestamp):
    nome = models.CharField(max_length=255)
    administrador = models.ForeignKey(User, on_delete=models.CASCADE, related_name='administrador_area')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.nome


class Endereco(models.Model):
    cep = models.CharField("CEP", max_length=9, blank=True, null=True)
    logradouro = models.CharField(max_length=100, blank=True, null=True)
    numero = models.CharField("Nº", max_length=20, blank=True, null=True)
    bairro = models.CharField(max_length=100, blank=True, null=True)
    cidade = models.CharField(max_length=20, blank=True, null=True)
    estado = models.CharField(max_length=2, blank=True, null=True)
    complemento = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        abstract = True


class AssinaturaDigital(Timestamp):
    nome = models.CharField(max_length=255)
    cargo = models.CharField(max_length=255)
    imagem = models.CharField(max_length=255)
    administrador = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='administrador_assinaturadigital')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.nome


class Conta(Timestamp):
    banco = models.CharField(max_length=255)
    agencia = models.CharField(max_length=20)
    conta = models.CharField(max_length=50)
    operacao = models.CharField(max_length=5, blank=True, null=True)
    titular = models.CharField(max_length=255)
    administrador = models.ForeignKey(User, on_delete=models.CASCADE, related_name='administrador_conta')

    def __str__(self):
        return self.titular


class Edicao(Endereco, Timestamp):
    edicao = models.CharField(max_length=30)
    logomarca = models.CharField(max_length=255, blank=True, null=True)
    inicio = models.DateField()
    termino = models.DateField()
    evento = models.ForeignKey(Evento, on_delete=models.CASCADE, related_name='evento_edicao')
    areas = models.ManyToManyField(Area, related_name='area_edicao')
    inicio_inscricao = models.DateField()
    termino_inscricao = models.DateField()
    certificado = models.BooleanField(default=False)
    palestra_obrigatoria = models.BooleanField(default=False)
    conta = models.ForeignKey(
        Conta, on_delete=models.CASCADE, related_name='conta_edicao', blank=True, null=True)

    class Meta:
        ordering = ['-created']

    def nome(self):
        return self.evento.sigla + " - " + self.edicao

    def status(self):
        today = date.today()
        if self.inicio_inscricao > today:
            return 'Inscrições não iniciadas'
        elif self.termino_inscricao >= today:
            return 'Inscrições abertas'
        elif self.inicio > today:
            return 'Inscrições encerradas'
        elif self.termino >= today:
            return 'Evento em andamento'
        else:
            return 'Evento Finalizado'

    def has_vagas(self):
        atividades_gerais = self.edicao_atividadegeral.count()
        if atividades_gerais:
            return True

        palestras = self.edicao_palestra.count()
        if palestras:
            return True

        minicursos = self.edicao_minicurso.all()
        for minicurso in minicursos:
            turmas = minicurso.minicurso_turma.all()
            for turma in turmas:
                if turma.vagas_disponiveis():
                    return True

        atividades = self.edicao_atividade.all()
        for atividade in atividades:
            turmas = atividade.atividade_turma.all()
            for turma in turmas:
                if turma.vagas_disponiveis():
                    return True

        return False

    def __str__(self):
        return self.nome()


class ModeloCertificado(Timestamp):
    descricao = models.CharField(max_length=100)
    background_frente = models.CharField(max_length=255)
    background_costa = models.CharField(max_length=255)
    administrador = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='administrador_modelocertificado')

    def __str__(self):
        return self.descricao


class ConfiguracaoCertificado(Timestamp):
    modelo = models.ForeignKey(
        ModeloCertificado, on_delete=models.CASCADE, related_name='modelo_configuracaocertificado')
    livro_certificado = models.CharField(max_length=30)
    quantidade_linha_pagina = models.PositiveSmallIntegerField(default=20)
    numero_inicio_certificado = models.PositiveSmallIntegerField(default=1)
    pagina_inicio_certificado = models.PositiveSmallIntegerField(default=1)
    assinatura_carimbo = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturacarimbo_configuracaocertificado',
        blank=True,
        null=True)
    edicao = models.OneToOneField(
        Edicao, on_delete=models.CASCADE, related_name='edicao_configuracaocertificado')


class ModeloCertificadoEvento(models.Model):
    texto = models.TextField()
    modelo = models.OneToOneField(
        ModeloCertificado, on_delete=models.CASCADE, related_name='modelo_certificadoevento')
    assinatura_diretor = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturadiretor_modelocertificadoevento',
        blank=True,
        null=True)
    assinatura_coordenador = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturacoordenador_modelocertificadoevento',
        blank=True,
        null=True)


class ModeloCertificadoMinicurso(models.Model):
    texto = models.TextField()
    modelo = models.OneToOneField(
        ModeloCertificado, on_delete=models.CASCADE, related_name='modelo_certificadominicurso')
    assinatura_diretor = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturadiretor_modelocertificadominicurso',
        blank=True,
        null=True)
    assinatura_coordenador = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturacoordenador_modelocertificadominicurso',
        blank=True,
        null=True)


class ModeloCertificadoPalestra(models.Model):
    texto = models.TextField()
    modelo = models.OneToOneField(
        ModeloCertificado, on_delete=models.CASCADE, related_name='modelo_certificadopalestra')
    assinatura_diretor = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturadiretor_modelocertificadopalestra',
        blank=True,
        null=True)
    assinatura_coordenador = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturacoordenador_modelocertificadopalestra',
        blank=True,
        null=True)


class ModeloCertificadoAtividade(models.Model):
    modelo = models.ForeignKey(
        ModeloCertificado, on_delete=models.CASCADE, related_name='modelo_certificadoatividade')
    assinatura_diretor = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturadiretor_modelocertificadoatividade',
        blank=True,
        null=True)
    assinatura_coordenador = models.ForeignKey(
        AssinaturaDigital,
        on_delete=models.CASCADE,
        related_name='assinaturacoordenador_modelocertificadoatividade',
        blank=True,
        null=True)


class Profissional(Timestamp):
    nome = models.CharField(max_length=100)
    administrador = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='administrador_profissional')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.nome


class AtividadeGeral(Timestamp):
    descricao = models.CharField(max_length=255)
    carga_horaria = models.PositiveSmallIntegerField()
    data = models.DateField()
    inicio = models.TimeField(default='00:00')
    termino = models.TimeField(default='23:59')
    edicao = models.ForeignKey(Edicao, on_delete=models.CASCADE, related_name='edicao_atividadegeral')

    def __str__(self):
        return self.descricao

    class Meta:
        ordering = ['data', 'inicio']


class Minicurso(Timestamp):
    nome = models.CharField(max_length=255)
    carga_horaria = models.PositiveSmallIntegerField()
    edicao = models.ForeignKey(Edicao, on_delete=models.CASCADE, related_name='edicao_minicurso')

    def __str__(self):
        return self.nome


class Palestra(Timestamp):
    nome = models.CharField(max_length=255)
    hora = models.TimeField()
    data = models.DateField()
    carga_horaria = models.PositiveSmallIntegerField()
    profissional = models.ForeignKey(
        Profissional, on_delete=models.CASCADE, related_name='profissional_palestra')
    edicao = models.ForeignKey(Edicao, on_delete=models.CASCADE, related_name='edicao_palestra')

    def __str__(self):
        return self.nome


class Atividade(Timestamp):
    nome = models.CharField(max_length=255)
    carga_horaria = models.PositiveSmallIntegerField()
    edicao = models.ForeignKey(Edicao, on_delete=models.CASCADE, related_name='edicao_atividade')

    def __str__(self):
        return self.nome


class Grupo(Timestamp):
    nome = models.CharField(max_length=255)
    edicao = models.ForeignKey(Edicao, on_delete=models.CASCADE, related_name='edicao_grupo')

    def __str__(self):
        return self.nome


class Turma(Timestamp):
    vagas = models.PositiveSmallIntegerField()


class TurmaMinicurso(Turma):
    minicurso = models.ForeignKey(Minicurso, on_delete=models.CASCADE, related_name='minicurso_turma')
    profissional = models.ForeignKey(
        Profissional, on_delete=models.CASCADE, related_name='profissional_minicurso')
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE, related_name='grupo_turmaminicurso')

    class Meta:
        ordering = ['-created']

    def nome(self):
        return '%s - %s' % (self.minicurso.nome, self.grupo.nome)

    def vagas_disponiveis(self):
        vagas_ocupadas = self.turma_certificadominicurso.count()
        return self.vagas - vagas_ocupadas

    def __str__(self):
        return '%s - %s' % (self.minicurso.nome, self.vagas)


class TurmaAtividade(Turma):
    atividade = models.ForeignKey(Atividade, on_delete=models.CASCADE, related_name='atividade_turma')
    grupo = models.ForeignKey(Grupo, on_delete=models.CASCADE, related_name='grupo_turmaatividade')

    class Meta:
        ordering = ['-created']

    def nome(self):
        return '%s - %s' % (self.atividade.nome, self.grupo.nome)

    def vagas_disponiveis(self):
        vagas_ocupadas = self.turma_certificadoatividade.count()
        return self.vagas - vagas_ocupadas

    def __str__(self):
        return '%s - %s' % (self.atividade.nome, self.vagas)


class Horario(Timestamp):
    data = models.DateField()
    inicio = models.TimeField()
    termino = models.TimeField()

    class Meta:
        abstract = True


class HorarioMinicurso(Horario):
    turma = models.ForeignKey(
        TurmaMinicurso, on_delete=models.CASCADE, related_name='turma_horariominicurso')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return '%s - %s, de %s até %s.' % (
            self.turma.nome(), self.data, self.inicio, self.termino)


class HorarioAtividade(Horario):
    turma = models.ForeignKey(
        TurmaAtividade, on_delete=models.CASCADE, related_name='turma_horarioatividade')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return '%s - %s, de %s até %s.' % (
            self.turma.nome(), self.data, self.inicio, self.termino)


class TipoInscricao(Timestamp):
    nome = models.CharField(max_length=50)
    preco_minicurso = models.FloatField(default=0)
    preco_palestra = models.FloatField(default=0)
    preco_atividade = models.FloatField(default=0)
    data_aumento = models.DateField(blank=True, null=True)
    preco_minicurso_aumento = models.FloatField(blank=True, null=True)
    preco_palestra_aumento = models.FloatField(blank=True, null=True)
    preco_atividade_aumento = models.FloatField(blank=True, null=True)
    edicao = models.ForeignKey(Edicao, on_delete=models.CASCADE, related_name='edicao_tipoinscricao')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.nome


class TipoDocumento(Timestamp):
    nome = models.CharField(max_length=100)
    tipo_inscricao = models.ForeignKey(
        TipoInscricao, on_delete=models.CASCADE, related_name='tipoinscricao_tipodocumento')

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.nome


class Inscricao(Timestamp):
    STATUS = (
        ('I', 'Incompleta'),
        ('C', 'Confirmada')
    )
    participante = models.ForeignKey(
        Participante, on_delete=models.CASCADE, related_name='participante_inscricao')
    tipo = models.ForeignKey(TipoInscricao, on_delete=models.CASCADE, related_name='tipo_inscricao')
    preco = models.FloatField(default=0)
    status = models.CharField(max_length=1, choices=STATUS, default='I')
    ignore_pending = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created']
        unique_together = ['participante', 'tipo']

    def documentos_enviados(self):
        return self.inscricao_documento.all()

    def documentos_necessarios(self):
        documentos_enviados = self.documentos_enviados().values_list('tipo')
        return self.tipo.tipoinscricao_tipodocumento.exclude(
            pk__in=documentos_enviados)

    def __str__(self):
        return '%s - %s - %s' % (
            self.tipo.edicao.nome(),
            self.tipo.nome,
            self.participante.get_full_name())


class Participacao(Timestamp):
    inscricao = models.ForeignKey(
        Inscricao, on_delete=models.CASCADE, related_name='inscricao_participacao')
    atividade_geral = models.ForeignKey(
        AtividadeGeral, on_delete=models.CASCADE, related_name='atividadegeral_participacao')

    class Meta:
        unique_together = ['inscricao', 'atividade_geral']


class Documento(Timestamp):
    inscricao = models.ForeignKey(
        Inscricao, on_delete=models.CASCADE, related_name='inscricao_documento')
    documento = models.CharField(max_length=255)
    valido = models.BooleanField(default=False)
    tipo = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE, related_name='tipo_tipodocumento')

    class Meta:
        ordering = ['-created']
        unique_together = ['inscricao', 'tipo']

    def __str__(self):
        return '%s - %s - %s' % (
            self.tipo.tipo_inscricao.edicao.nome(),
            self.tipo.nome,
            self.inscricao.participante.get_full_name())


class Certificado(Timestamp):
    nome_certificado = models.CharField(max_length=255, blank=True, null=True)
    numero = models.PositiveSmallIntegerField(blank=True, null=True)
    data_emissao = models.DateField()
    disponivel = models.BooleanField(default=False)
    token = models.CharField(max_length=255)

    def __str__(self):
        return self.token


class CertificadoEvento(Certificado):
    inscricao = models.OneToOneField(
        Inscricao, on_delete=models.CASCADE, related_name='inscricao_certificadoevento')


class CertificadoMinicurso(Certificado):
    turma = models.ForeignKey(
        TurmaMinicurso, on_delete=models.CASCADE, related_name='turma_certificadominicurso')
    inscricao = models.ForeignKey(
        Inscricao, on_delete=models.CASCADE, related_name='inscricao_certificadominicurso')

    class Meta:
        ordering = ['-created']

    def pagina(self):
        conf = self.turma.minicurso.edicao.edicao_configuracaocertificado
        if conf:
            resultado = int(self.numero / conf.quantidade_linha_pagina +
                            conf.pagina_inicio_certificado)
            return resultado
        return 0

    def __str__(self):
        return '%s - %s - %s' % (
            self.token,
            self.turma.minicurso.nome,
            self.inscricao.participante.get_full_name())


class CertificadoPalestra(Certificado):
    palestra = models.ForeignKey(
        Palestra, on_delete=models.CASCADE, related_name='palestra_certificadopalestra')
    inscricao = models.ForeignKey(
        Inscricao, on_delete=models.CASCADE, related_name='inscricao_certificadopalestra')

    class Meta:
        ordering = ['-created']

    def pagina(self):
        conf = self.palestra.edicao.edicao_configuracaocertificado
        if conf:
            resultado = int(self.numero / conf.quantidade_linha_pagina +
                            conf.pagina_inicio_certificado)
            return resultado
        return 0

    def __str__(self):

        return '%s - %s - %s' % (
            self.token,
            self.palestra.nome,
            self.inscricao.participante.get_full_name())


class CertificadoAtividade(Certificado):
    turma = models.ForeignKey(
        TurmaAtividade, on_delete=models.CASCADE, related_name='turma_certificadoatividade')
    inscricao = models.ForeignKey(
        Inscricao, on_delete=models.CASCADE, related_name='inscricao_certificadoatividade')

    class Meta:
        ordering = ['-created']

    def pagina(self):
        conf = self.turma.atividade.edicao.edicao_configuracaocertificado
        if conf:
            resultado = int(self.numero / conf.quantidade_linha_pagina +
                            conf.pagina_inicio_certificado)
            return resultado
        return 0

    def __str__(self):
        return '%s - %s - %s' % (
            self.token,
            self.turma.atividade.nome,
            self.inscricao.participante.get_full_name())
