# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers

from apps.core.serializers import UserSerializer
from .models import Participante


class ParticipanteSerializer(UserSerializer):
    cpf = serializers.CharField()
    data_nascimento = serializers.DateField()

    class Meta:
        model = Participante
        fields = ['email', 'first_name', 'last_name', 'cpf',
                  'data_nascimento', 'password', 'confirm_password']

    def create(self, validated_data):
        validated_data['username'] = validated_data['email']
        del validated_data['confirm_password']
        participante = Participante(**validated_data)
        participante.set_password(validated_data['password'])
        participante.save()
        return participante


class ParticipanteUpdateSerializer(serializers.ModelSerializer):
    cpf = serializers.CharField()
    password = serializers.CharField(
        write_only=True, required=False, allow_blank=True)
    confirm_password = serializers.CharField(
        write_only=True, required=False, allow_blank=True)

    class Meta:
        model = Participante
        fields = ['first_name', 'last_name', 'cpf',
                  'data_nascimento', 'password', 'confirm_password']

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get(
            'first_name', instance.first_name)
        instance.last_name = validated_data.get(
            'last_name', instance.last_name)
        instance.cpf = validated_data.get('cpf', instance.cpf)
        instance.data_nascimento = validated_data.get(
            'data_nascimento', instance.data_nascimento)
        if 'password' in validated_data:
            instance.set_password(validated_data.get(
                'password', instance.password))
        instance.save()
        return instance

    def validate(self, data):
        if 'password' in data or 'confirm_password' in data:
            if 'password' not in data or 'confirm_password' not in data:
                raise serializers.ValidationError(
                    'Por favor, se deseja mudar sua senha, entre com uma ' +
                    'senha e a confirme.')

            if data['password'] != data['confirm_password']:
                raise serializers.ValidationError(
                    'As senhas não correspondem.')

            if len(data['password']) < 1:
                del data['password']
                del data['confirm_password']
            elif len(data['password']) < 8:
                raise serializers.ValidationError(
                    'Por favor, insira uma senha com pelo menos 8 caracteres.')
        return data
