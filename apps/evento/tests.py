# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Importando do Python

import hashlib

from datetime import date

# Importando do Django

from django.contrib.auth.models import User
from django.test import TestCase

# Importando de Terceiros

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

# Importando do rease

from .models import *
from .serializers import *
from apps.administrador.serializers import *


class EdicaoTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        admin = User(username='admin', is_staff=True)
        admin.set_password('12345678')
        admin.save()
        self.admin = admin
        self.token = Token.objects.create(user=self.admin)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.evento = Evento.objects.create(
            nome='TESTE', sigla='TST', administrador=admin)

    def test_edicao_finalizada(self):
        data = self.client.post(
            '/administrador/edicoes/',
            {
                'edicao': 'I',
                'inicio': '2000-12-31',
                'termino': '2001-01-31',
                'inicio_inscricao': '2000-12-31',
                'termino_inscricao': '2001-01-31',
                'cep': '64601-237',
                'areas': [],
                'evento': self.evento.pk
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        self.assertEquals('Evento Finalizado', data.data['status'])

    def test_edicao_encerrada(self):
        data = self.client.post(
            '/administrador/edicoes/',
            {
                'edicao': 'II',
                'inicio': '2016-10-20',
                'termino': '2016-10-30',
                'inicio_inscricao': '2000-12-31',
                'termino_inscricao': '2016-09-26',
                'cep': '64601-237',
                'areas': [],
                'evento': self.evento.pk
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        self.assertEquals('Inscrições encerradas', data.data['status'])

    def test_edicao_aberta(self):
        data = self.client.post(
            '/administrador/edicoes/',
            {
                'edicao': 'I',
                'inicio': '2099-12-31',
                'termino': '2100-01-31',
                'inicio_inscricao': '2016-09-25',
                'termino_inscricao': '2016-12-31',
                'cep': '64601-237',
                'areas': [],
                'evento': self.evento.pk
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        self.assertEquals('Inscrições abertas', data.data['status'])


class InscricaoParticipanteTestCase(TestCase):

    def setUp(self):
        admin = User(username='admin', is_staff=True)
        admin.set_password('12345678')
        admin.save()
        participante = Participante(username='dayvidemerson@rease.com',
                                    email='dayvidemerson@rease.com', cpf='063.611.473-20')
        participante.set_password('12345678')
        participante.save()
        self.client = APIClient()
        self.client_admin = APIClient()
        self.admin = admin
        self.participante = participante
        self.token = Token.objects.create(user=participante)
        self.token_admin = Token.objects.create(user=admin)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.client_admin.credentials(
            HTTP_AUTHORIZATION='Token ' + self.token_admin.key)
        self.evento = Evento.objects.create(
            nome='TESTE', sigla='TST', administrador=self.admin)
        self.edicao = Edicao.objects.create(
            edicao='TESTANDO',
            inicio=date(2016, 10, 10),
            termino=date(2016, 11, 11),
            inicio_inscricao=date(2016, 10, 10),
            termino_inscricao=date(2016, 11, 11),
            evento=self.evento
        )
        self.profissional = Profissional.objects.create(
            nome='Teste', administrador=self.admin)
        self.tipo_inscricao = TipoInscricao.objects.create(
            nome='Estudante',
            edicao=self.edicao,
            preco_minicurso=10,
            preco_atividade=10,
            preco_palestra=10,
            data_aumento='2016-10-10',
            preco_minicurso_aumento=15,
            preco_atividade_aumento=15,
            preco_palestra_aumento=15
        )
        self.tipo_inscricao2 = TipoInscricao.objects.create(
            nome='Profissional',
            edicao=self.edicao,
            preco_minicurso=20,
            preco_atividade=20,
            preco_palestra=20,
            data_aumento='2016-10-10',
            preco_minicurso_aumento=30,
            preco_atividade_aumento=30,
            preco_palestra_aumento=30
        )
        self.tipo_documento = TipoDocumento.objects.create(
            nome='Carteira de Estudante', tipo_inscricao=self.tipo_inscricao2)
        self.minicurso = Minicurso.objects.create(
            nome='Django', carga_horaria=10, edicao=self.edicao)
        self.atividade = Atividade.objects.create(
            nome='Olimpiada', carga_horaria=2, edicao=self.edicao)
        self.grupo1 = Grupo.objects.create(nome='1', edicao=self.edicao)
        self.grupo2 = Grupo.objects.create(nome='2', edicao=self.edicao)
        self.grupo3 = Grupo.objects.create(nome='3', edicao=self.edicao)
        self.grupo4 = Grupo.objects.create(nome='4', edicao=self.edicao)
        self.grupo5 = Grupo.objects.create(nome='5', edicao=self.edicao)
        self.grupo6 = Grupo.objects.create(nome='6', edicao=self.edicao)
        self.grupo7 = Grupo.objects.create(nome='7', edicao=self.edicao)
        self.turma_atividade = TurmaAtividade.objects.create(
            vagas=10, atividade=self.atividade, grupo=self.grupo1)
        self.turma_atividade_sem_vagas = TurmaAtividade.objects.create(
            vagas=0, atividade=self.atividade, grupo=self.grupo2)
        self.turma_atividade_uma_vaga = TurmaAtividade.objects.create(
            vagas=1, atividade=self.atividade, grupo=self.grupo3)
        self.turma_minicurso1 = TurmaMinicurso.objects.create(
            vagas=10, minicurso=self.minicurso, profissional=self.profissional, grupo=self.grupo4)
        self.turma_minicurso2 = TurmaMinicurso.objects.create(
            vagas=15, minicurso=self.minicurso, profissional=self.profissional, grupo=self.grupo5)
        self.turma_minicurso_sem_vagas = TurmaMinicurso.objects.create(
            vagas=0, minicurso=self.minicurso, profissional=self.profissional, grupo=self.grupo6)
        self.turma_minicurso_uma_vaga = TurmaMinicurso.objects.create(
            vagas=1, minicurso=self.minicurso, profissional=self.profissional, grupo=self.grupo7)
        self.palestra1 = Palestra.objects.create(
            nome='Teste', profissional=self.profissional, hora='20:00', data='2015-10-10', edicao=self.edicao, carga_horaria=1)
        self.palestra2 = Palestra.objects.create(
            nome='Teste2', profissional=self.profissional, hora='21:00', data='2015-10-10', edicao=self.edicao, carga_horaria=1)

        self.competicao = Competicao.objects.create(
            nome='Maratona de Programação',
            numero_equipes=15,
            numero_participantes_equipe=2,
            carga_horaria=20,
            edicao=self.edicao,
        )

    def test_create_inscricao(self):
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao.pk,
                'certificados_minicursos': [self.turma_minicurso1.pk, self.turma_minicurso2.pk],
                'certificados_atividades': [self.turma_atividade.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        self.assertEquals(60.0, float(data.data['preco']))
        self.assertEquals('Confirmada', data.data['status'])

        certificado1 = CertificadoMinicurso.objects.get(
            turma=self.turma_minicurso1)
        certificado2 = CertificadoMinicurso.objects.get(
            turma=self.turma_minicurso2)
        certificado3 = CertificadoAtividade.objects.get(
            turma=self.turma_atividade)
        certificado4 = CertificadoPalestra.objects.get(palestra=self.palestra1)
        certificado5 = CertificadoPalestra.objects.get(palestra=self.palestra2)

        string1 = 'Inscrição %s Minicurso %s' % (
            data.data['pk'], certificado1.turma.pk)
        string2 = 'Inscrição %s Minicurso %s' % (
            data.data['pk'], certificado2.turma.pk)
        string3 = 'Inscrição %s Atividade %s' % (
            data.data['pk'], certificado3.turma.pk)
        string4 = 'Inscrição %s Palestra %s' % (
            data.data['pk'], certificado4.palestra.pk)
        string5 = 'Inscrição %s Palestra %s' % (
            data.data['pk'], certificado5.palestra.pk)

        code1 = hashlib.md5(string1.encode()).hexdigest()
        code2 = hashlib.md5(string2.encode()).hexdigest()
        code3 = hashlib.md5(string3.encode()).hexdigest()
        code4 = hashlib.md5(string4.encode()).hexdigest()
        code5 = hashlib.md5(string5.encode()).hexdigest()

        self.assertEquals(code1, certificado1.token)
        self.assertEquals(code2, certificado2.token)
        self.assertEquals(code3, certificado3.token)
        self.assertEquals(code4, certificado4.token)
        self.assertEquals(code5, certificado5.token)

    def test_create_inscricao_sem_vagas(self):
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao.pk,
                'certificados_minicursos': [self.turma_minicurso_sem_vagas.pk],
                'certificados_atividades': [self.turma_atividade_sem_vagas.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals('Verifique o número de vagas, do(s) minicurso(s) escolhido(s).', data.data[
                          'certificados_minicursos'][0])
        self.assertEquals('Verifique o número de vagas, da(s) atividade(s) escolhida(s).', data.data[
                          'certificados_atividades'][0])

    def test_create_inscricao_uma_vaga(self):
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao.pk,
                'certificados_minicursos': [self.turma_minicurso_uma_vaga.pk],
                'certificados_atividades': [self.turma_atividade_uma_vaga.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)

    def test_create_mais_uma_inscricao_na_mesma_edicao(self):
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao.pk,
                'certificados_minicursos': [self.turma_minicurso1.pk, self.turma_minicurso2.pk],
                'certificados_atividades': [self.turma_atividade.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao.pk,
                'certificados_minicursos': [self.turma_minicurso1.pk, self.turma_minicurso2.pk],
                'certificados_atividades': [self.turma_atividade.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals(400, data.status_code)

    def test_ativar_inscricao(self):
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao2.pk,
                'certificados_minicursos': [self.turma_minicurso1.pk, self.turma_minicurso2.pk],
                'certificados_atividades': [self.turma_atividade.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        self.assertEquals('Incompleta', data.data['status'])
        inscricao_ativada = self.client_admin.put(
            '/administrador/inscricoes/' + str(data.data['pk']) + '/',
            {
                'active': True
            },
            format='json'
        )
        self.assertEquals('Confirmada', inscricao_ativada.data['status'])
        self.assertEquals(201, data.status_code)

    def test_create_equipe(self):
        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao.pk,
                'certificados_minicursos': [self.turma_minicurso1.pk, self.turma_minicurso2.pk],
                'certificados_atividades': [self.turma_atividade.pk],
                'certificados_palestras': True
            },
            format='json'
        )
        self.assertEquals(201, data.status_code)
        self.assertEquals(60.0, float(data.data['preco']))
        self.assertEquals('Confirmada', data.data['status'])

        data = self.client.post(
            '/participante/equipes/',
            {
                'lider': data.data['pk'],
                'nome': 'Destruidor de Sonhos',
                'competicao': self.competicao.pk
            },
            format='json'
        )

        self.assertEquals(201, data.status_code)
        self.assertEquals('DESTRMARAT', data.data['codigo'])
        self.assertEquals(1, data.data['vagas'])

        data = self.client.post(
            '/participante/inscricoes/',
            {
                'tipo': self.tipo_inscricao2.pk,
                'certificados_minicursos': [self.turma_minicurso1.pk, self.turma_minicurso2.pk],
                'certificados_atividades': [self.turma_atividade.pk],
                'certificados_palestras': True
            },
            format='json'
        )

        pk_inscricao = data.data['pk']

        data = self.client.post(
            '/participante/certificados/competicao/',
            {
                'inscricao': data.data['pk'],
                'codigo': 'DESTRMARAT',
            },
            format='json'
        )

        self.assertEquals(201, data.status_code)
        equipe = Equipe.objects.get(codigo='DESTRMARAT')
        self.assertEquals(0, equipe.vagas_disponiveis())

        data = self.client.post(
            '/participante/certificados/competicao/',
            {
                'inscricao': pk_inscricao,
                'codigo': 'DESTRMARAT',
            },
            format='json'
        )

        self.assertEquals(400, data.status_code)
