# -*- coding: utf-8 -*-
from base64 import b64decode

from datetime import date

from rest_framework import serializers

from apps.evento.models import CertificadoAtividade
from apps.evento.models import CertificadoEvento
from apps.evento.models import CertificadoMinicurso
from apps.evento.models import CertificadoPalestra
from apps.evento.models import Documento
from apps.evento.models import Inscricao
from apps.evento.models import Palestra
from apps.evento.models import TipoDocumento
from apps.evento.models import TipoInscricao
from apps.evento.models import TurmaAtividade
from apps.evento.models import TurmaMinicurso
from rease.settings import MEDIA_ROOT
from rease.settings import MEDIA_URL


class EventoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.CharField(read_only=True)
    sigla = serializers.CharField(read_only=True)


class AreaSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.CharField(read_only=True)


class EnderecoSerializer(serializers.Serializer):
    cep = serializers.CharField(read_only=True)
    logradouro = serializers.CharField(read_only=True)
    numero = serializers.CharField(read_only=True)
    bairro = serializers.CharField(read_only=True)
    cidade = serializers.CharField(read_only=True)
    estado = serializers.CharField(read_only=True)
    complemento = serializers.CharField(read_only=True)


class ContaSerializer(serializers.Serializer):
    banco = serializers.CharField(read_only=True)
    agencia = serializers.CharField(read_only=True)
    conta = serializers.CharField(read_only=True)
    operacao = serializers.CharField(read_only=True)
    titular = serializers.CharField(read_only=True)


class EdicaoSerializer(EnderecoSerializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.SerializerMethodField()
    edicao = serializers.CharField(read_only=True)
    logomarca = serializers.CharField(read_only=True)
    inicio = serializers.DateField(read_only=True)
    termino = serializers.DateField(read_only=True)
    inicio_inscricao = serializers.DateField(read_only=True)
    termino_inscricao = serializers.DateField(read_only=True)
    evento = serializers.PrimaryKeyRelatedField(read_only=True)
    areas = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    status = serializers.SerializerMethodField()

    def get_nome(self, obj):
        return obj.nome()

    def get_status(self, obj):
        return obj.status()


class MinicursoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.CharField(read_only=True)
    carga_horaria = serializers.IntegerField(read_only=True)
    edicao = serializers.PrimaryKeyRelatedField(read_only=True)


class AtividadeSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.CharField(read_only=True)
    carga_horaria = serializers.IntegerField(read_only=True)
    edicao = serializers.PrimaryKeyRelatedField(read_only=True)


class AtividadeGeralSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    descricao = serializers.CharField(read_only=True)
    data = serializers.DateField(read_only=True)
    inicio = serializers.TimeField(read_only=True)
    termino = serializers.TimeField(read_only=True)
    edicao = serializers.PrimaryKeyRelatedField(read_only=True)


class TurmaMinicursoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    minicurso = serializers.SerializerMethodField()
    vagas = serializers.SerializerMethodField()
    profissional = serializers.SerializerMethodField()
    horarios = serializers.SerializerMethodField()

    def get_minicurso(self, obj):
        return obj.minicurso.nome

    def get_profissional(self, obj):
        return obj.profissional.nome

    def get_horarios(self, obj):
        serializer = HorarioSerializer(
            obj.turma_horariominicurso.all(), many=True)
        return serializer.data

    def get_vagas(self, obj):
        return obj.vagas_disponiveis()


class TurmaAtividadeSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    atividade = serializers.SerializerMethodField()
    descricao = serializers.SerializerMethodField()
    vagas = serializers.SerializerMethodField()
    horarios = serializers.SerializerMethodField()

    def get_atividade(self, obj):
        return obj.atividade.nome

    def get_descricao(self, obj):
        return obj.atividade.descricao

    def get_horarios(self, obj):
        serializer = HorarioSerializer(
            obj.turma_horarioatividade.all(), many=True)
        return serializer.data

    def get_vagas(self, obj):
        return obj.vagas_disponiveis()


class GrupoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome = serializers.CharField(read_only=True)
    minicursos = serializers.SerializerMethodField()
    atividades = serializers.SerializerMethodField()

    def get_minicursos(self, obj):
        minicursos = []
        for minicurso in obj.grupo_turmaminicurso.all():
            if minicurso.vagas_disponiveis() > 0:
                minicursos.append(minicurso)
        serializer = TurmaMinicursoSerializer(minicursos, many=True)
        return serializer.data

    def get_atividades(self, obj):
        atividades = []
        for atividade in obj.grupo_turmaatividade.all():
            if atividade.vagas_disponiveis() > 0:
                atividades.append(atividade)
        serializer = TurmaAtividadeSerializer(atividades, many=True)
        return serializer.data


class HorarioSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    data = serializers.DateField(read_only=True)
    inicio = serializers.TimeField(read_only=True)
    termino = serializers.TimeField(read_only=True)


class PalestraSerializer(serializers.Serializer):
    nome = serializers.CharField(read_only=True)
    hora = serializers.TimeField(read_only=True)
    data = serializers.DateField(read_only=True)
    profissional = serializers.SerializerMethodField()
    edicao = serializers.PrimaryKeyRelatedField(read_only=True)

    def get_profissional(self, obj):
        return obj.profissional.nome


class CertificadoSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    nome_certificado = serializers.CharField(
        max_length=255, required=False, allow_blank=True, allow_null=True)
    token = serializers.SerializerMethodField()
    disponivel = serializers.BooleanField(read_only=True)

    def get_token(self, obj):
        return obj.token


class CertificadoMinicursoSerializer(CertificadoSerializer):
    evento = serializers.SerializerMethodField()
    turma = serializers.SerializerMethodField()

    def get_evento(self, obj):
        return obj.turma.grupo.edicao.nome()

    def get_turma(self, obj):
        return obj.turma.minicurso.nome


class CertificadoPalestraSerializer(CertificadoSerializer):
    evento = serializers.SerializerMethodField()
    palestra = serializers.SerializerMethodField()

    def get_evento(self, obj):
        return obj.palestra.edicao.nome()

    def get_palestra(self, obj):
        return obj.palestra.nome


class CertificadoEventoSerializer(CertificadoSerializer):
    descricao = serializers.SerializerMethodField()
    evento = serializers.SerializerMethodField()

    def get_descricao(self, obj):
        return '%s - %s' % (obj.inscricao.tipo.edicao.evento.nome, obj.inscricao.tipo.edicao.edicao)

    def get_evento(self, obj):
        return obj.inscricao.tipo.edicao.nome()


class CertificadoAtividadeSerializer(CertificadoSerializer):
    evento = serializers.SerializerMethodField()
    turma = serializers.SerializerMethodField()

    def get_evento(self, obj):
        return obj.turma.grupo.edicao.nome()

    def get_turma(self, obj):
        return obj.turma.atividade.nome


class TipoInscricaoSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    nome = serializers.CharField(read_only=True)
    palestra_obrigatoria = serializers.SerializerMethodField()
    preco_minicurso = serializers.SerializerMethodField()
    preco_palestra = serializers.SerializerMethodField()
    preco_atividade = serializers.SerializerMethodField()

    def get_palestra_obrigatoria(self, obj):
        return obj.edicao.palestra_obrigatoria

    def get_preco_minicurso(self, obj):
        if obj.data_aumento and obj.data_aumento <= date.today():
            return obj.preco_minicurso_aumento
        return obj.preco_minicurso

    def get_preco_palestra(self, obj):
        if obj.data_aumento and obj.data_aumento <= date.today():
            return obj.preco_palestra_aumento
        return obj.preco_palestra

    def get_preco_atividade(self, obj):
        if obj.data_aumento and obj.data_aumento <= date.today():
            return obj.preco_atividade_aumento
        return obj.preco_atividade


class TipoDocumentoSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    nome = serializers.CharField(read_only=True)
    tipo_inscricao = serializers.PrimaryKeyRelatedField(read_only=True)


class InscricaoSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    preco = serializers.FloatField(read_only=True)
    status = serializers.SerializerMethodField()
    minicursos = serializers.SerializerMethodField()
    palestras = serializers.SerializerMethodField()
    atividades = serializers.SerializerMethodField()
    documentos_enviados = serializers.SerializerMethodField()
    documentos_necessarios = serializers.SerializerMethodField()
    atividades_gerais = serializers.SerializerMethodField()
    edicao = serializers.SerializerMethodField()
    tipo = serializers.PrimaryKeyRelatedField(
        queryset=TipoInscricao.objects.all())
    conta = serializers.SerializerMethodField()
    certificados_minicursos = serializers.PrimaryKeyRelatedField(
        queryset=TurmaMinicurso.objects.all(), many=True, write_only=True)
    certificados_palestras = serializers.BooleanField(write_only=True)
    certificado_evento = serializers.BooleanField(write_only=True)
    certificados_atividades = serializers.PrimaryKeyRelatedField(
        queryset=TurmaAtividade.objects.all(), many=True, write_only=True)

    class Meta:
        model = Inscricao
        fields = ['pk', 'tipo', 'preco', 'status', 'minicursos', 'atividades',
                  'palestras', 'documentos_enviados', 'documentos_necessarios',
                  'atividades_gerais', 'certificados_minicursos', 'edicao', 'conta',
                  'certificados_palestras', 'certificados_atividades', 'certificado_evento']

    def create(self, validated_data):
        minicursos_data = []
        atividades_data = []

        certificado_evento = validated_data.pop('certificado_evento')

        palestras_data = validated_data.pop('certificados_palestras')

        if 'certificados_minicursos' in validated_data:
            minicursos_data = validated_data.pop('certificados_minicursos')

        if 'certificados_atividades' in validated_data:
            atividades_data = validated_data.pop('certificados_atividades')

        inscricao = Inscricao.objects.create(**validated_data)

        if certificado_evento:
            CertificadoEvento.objects.create(inscricao=inscricao)

        if minicursos_data:
            for minicurso_data in minicursos_data:
                CertificadoMinicurso.objects.create(
                    inscricao=inscricao, turma=minicurso_data)

        if atividades_data:
            for atividade_data in atividades_data:
                CertificadoAtividade.objects.create(
                    inscricao=inscricao, turma=atividade_data)

        if inscricao.tipo.edicao.palestra_obrigatoria or palestras_data:
            palestras = Palestra.objects.filter(
                edicao=inscricao.tipo.edicao).order_by('data', 'hora')
            for palestra in palestras:
                CertificadoPalestra.objects.create(
                    inscricao=inscricao, palestra=palestra)
        return inscricao

    def validate(self, data):
        if (not data['certificado_evento'] and
                not data['certificados_minicursos'] and
                not data['certificados_atividades'] and
                not data['certificados_palestras']):
            raise serializers.ValidationError(
                'Verifique se as opções desejadas estão selecionadas.')
        return data

    def validate_tipo(self, value):
        pk = self.context['request'].user.pk
        if Inscricao.objects.filter(participante__pk=pk, tipo=value):
            raise serializers.ValidationError(
                'Você já está inscrito neste evento.')
        return value

    def validate_certificados_minicursos(self, value):
        for turma in value:
            if turma.vagas_disponiveis() < 1:
                raise serializers.ValidationError(
                    'Verifique o número de vagas, do(s) minicurso(s) ' +
                    'escolhido(s).')
        return value

    def validate_certificados_atividades(self, value):
        for turma in value:
            if turma.vagas_disponiveis() < 1:
                raise serializers.ValidationError(
                    'Verifique o número de vagas, da(s) atividade(s) ' +
                    'escolhida(s).')
        return value

    def get_status(self, obj):
        return obj.get_status_display()

    def get_edicao(self, obj):
        return obj.tipo.edicao.nome()

    def get_conta(self, obj):
        conta = None
        if obj.tipo.edicao.conta:
            conta = ContaSerializer(obj.tipo.edicao.conta).data
        return conta

    def get_minicursos(self, obj):
        certificados = obj.inscricao_certificadominicurso.all()
        turmas = []
        for certificado in certificados:
            turmas.append(certificado.turma)
        serializer = TurmaMinicursoSerializer(turmas, many=True)
        return serializer.data

    def get_atividades(self, obj):
        certificados = obj.inscricao_certificadoatividade.all()
        turmas = []
        for certificado in certificados:
            turmas.append(certificado.turma)
        serializer = TurmaAtividadeSerializer(turmas, many=True)
        return serializer.data

    def get_atividades_gerais(self, obj):
        atividades = obj.tipo.edicao.edicao_atividadegeral.all()
        serializer = AtividadeGeralSerializer(atividades, many=True)
        return serializer.data

    def get_palestras(self, obj):
        palestras = []
        if obj.inscricao_certificadopalestra.count():
            palestras = Palestra.objects.filter(edicao=obj.tipo.edicao)
        serializer = PalestraSerializer(palestras, many=True)
        return serializer.data

    def get_documentos_enviados(self, obj):
        documentos = obj.documentos_enviados()
        serializer = DocumentoSerializer(documentos, many=True)
        return serializer.data

    def get_documentos_necessarios(self, obj):
        documentos = obj.documentos_necessarios()
        serializer = TipoDocumentoSerializer(documentos, many=True)
        return serializer.data


class DocumentoSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    inscricao = serializers.PrimaryKeyRelatedField(
        queryset=Inscricao.objects.all())
    tipo = serializers.PrimaryKeyRelatedField(
        queryset=TipoDocumento.objects.all())
    nome_tipo = serializers.SerializerMethodField()
    documento = serializers.CharField()
    valido = serializers.BooleanField(read_only=True)

    class Meta:
        model = Documento
        fields = ['pk', 'inscricao', 'tipo',
                  'nome_tipo', 'documento', 'valido']

    def get_nome_tipo(self, obj):
        return obj.tipo.nome

    def validate_documento(self, data):
        if 'http' not in data:
            try:
                last = Documento.objects.latest('pk').pk
            except:
                last = 0

            value = data
            data = '%sdocumento/documento-%s.png' % (
                MEDIA_URL, last)

            if 'base64,' in value:
                value = value.split('base64,')[1]

            path = '%sdocumento/documento-%s.png' % (MEDIA_ROOT, last)
            fh = open(path, 'wb')
            fh.write(b64decode(value))
            fh.close()
        return data
