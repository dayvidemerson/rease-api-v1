# -*- coding: utf-8 -*-
import os.path

from datetime import date

from django.core.mail import EmailMessage
from django.db.models import Sum
from django.template import Context
from django.template.loader import get_template

from pydf import generate_pdf
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import *
from apps.evento.models import *
from apps.evento.permissions import IsParticipante
from rease.settings import API_HOST
from rease.settings import MEDIA_ROOT
from rease.settings import MEDIA_URL


@api_view(['GET'])
@permission_classes([])
def generate_certificado(request, token):
    assinatura_carimbo = None
    modelo = None

    certificado = CertificadoMinicurso.objects.filter(
        token__iexact=token).first()
    if certificado and certificado.disponivel:
        edicao = certificado.turma.minicurso.edicao
        if (edicao.certificado and
                hasattr(edicao, 'edicao_configuracaocertificado')):

            configuracao = edicao.edicao_configuracaocertificado
            modelo = configuracao.modelo
            assinatura_carimbo = configuracao.assinatura_carimbo

            modelo = modelo.modelo_certificadominicurso
            inscricao = certificado.inscricao
            modelo.texto = modelo.texto.replace(
                '<<nome>>', certificado.nome_certificado)
            modelo.texto = modelo.texto.replace(
                '<<atividade>>', certificado.turma.minicurso.nome)
            modelo.texto = modelo.texto.replace(
                '<<evento>>',
                inscricao.tipo.edicao.edicao + ' ' +
                inscricao.tipo.edicao.evento.nome)
            modelo.texto = modelo.texto.replace(
                '<<inicio>>',
                inscricao.tipo.edicao.inicio.strftime('%d/%m/%Y'))
            modelo.texto = modelo.texto.replace(
                '<<termino>>',
                inscricao.tipo.edicao.termino.strftime('%d/%m/%Y'))
            modelo.texto = modelo.texto.replace('<<carga_horaria>>', str(
                certificado.turma.minicurso.carga_horaria))

    if not certificado:
        certificado = CertificadoPalestra.objects.filter(
            token__iexact=token).first()
        if certificado and certificado.disponivel:
            edicao = certificado.palestra.edicao
            if (edicao.certificado and
                    hasattr(edicao, 'edicao_configuracaocertificado')):

                configuracao = edicao.edicao_configuracaocertificado
                modelo = configuracao.modelo
                assinatura_carimbo = configuracao.assinatura_carimbo

                modelo = modelo.modelo_certificadopalestra
                inscricao = certificado.inscricao
                modelo.texto = modelo.texto.replace(
                    '<<nome>>', certificado.nome_certificado)
                modelo.texto = modelo.texto.replace(
                    '<<palestra>>', certificado.palestra.nome)
                modelo.texto = modelo.texto.replace(
                    '<<evento>>',
                    inscricao.tipo.edicao.edicao + ' ' +
                    inscricao.tipo.edicao.evento.nome)
                modelo.texto = modelo.texto.replace(
                    '<<data>>', certificado.palestra.data.strftime('%d/%m/%Y'))
                modelo.texto = modelo.texto.replace(
                    '<<carga_horaria>>', str(certificado.palestra.carga_horaria))

    if not certificado:
        certificado = CertificadoAtividade.objects.filter(
            token__iexact=token).first()
        if certificado and certificado.disponivel:
            edicao = certificado.turma.atividade.edicao
            if (edicao.certificado and
                    hasattr(edicao, 'edicao_configuracaocertificado')):

                configuracao = edicao.edicao_configuracaocertificado
                modelo = configuracao.modelo
                assinatura_carimbo = configuracao.assinatura_carimbo

                modelo = modelo.modelo_certificadominicurso
                inscricao = certificado.inscricao
                modelo.texto = modelo.texto.replace(
                    '<<nome>>', certificado.nome_certificado)
                modelo.texto = modelo.texto.replace(
                    '<<atividade>>', certificado.turma.atividade.nome)
                modelo.texto = modelo.texto.replace(
                    '<<evento>>',
                    inscricao.tipo.edicao.edicao + ' ' +
                    inscricao.tipo.edicao.evento.nome)
                modelo.texto = modelo.texto.replace(
                    '<<inicio>>',
                    inscricao.tipo.edicao.inicio.strftime('%d/%m/%Y'))
                modelo.texto = modelo.texto.replace(
                    '<<termino>>',
                    inscricao.tipo.edicao.termino.strftime('%d/%m/%Y'))
                modelo.texto = modelo.texto.replace('<<carga_horaria>>', str(
                    certificado.turma.atividade.carga_horaria))

    if not certificado:
        certificado = CertificadoEvento.objects.filter(
            token__iexact=token).first()
        if certificado and certificado.disponivel:
            inscricao = certificado.inscricao
            edicao = inscricao.tipo.edicao

            if (edicao.certificado and
                    hasattr(edicao, 'edicao_configuracaocertificado')):
                configuracao = edicao.edicao_configuracaocertificado
                modelo = configuracao.modelo
                assinatura_carimbo = configuracao.assinatura_carimbo

                modelo = modelo.modelo_certificadoevento
                modelo.texto = modelo.texto.replace(
                    '<<nome>>', certificado.nome_certificado)
                modelo.texto = modelo.texto.replace(
                    '<<evento>>',
                    edicao.edicao + ' ' +
                    edicao.evento.nome)
                modelo.texto = modelo.texto.replace(
                    '<<inicio>>', edicao.inicio.strftime('%d/%m/%Y'))
                modelo.texto = modelo.texto.replace(
                    '<<termino>>', edicao.termino.strftime('%d/%m/%Y'))
                lista_carga_horaria = inscricao.inscricao_participacao.values_list(
                    'atividade_geral__carga_horaria')
                carga_horaria = 0
                for c in lista_carga_horaria:
                    carga_horaria += c[0]
                modelo.texto = modelo.texto.replace(
                    '<<carga_horaria>>', str(carga_horaria))

    if certificado and certificado.disponivel:
        template = get_template('certificado.html')
        context = Context({
            'assinatura_carimbo': assinatura_carimbo,
            'modelo': modelo,
            'configuracao': configuracao,
            'certificado': certificado
        })
        html = template.render(context)
        pdf = generate_pdf(
            html,
            orientation='Landscape',
            margin_bottom='0',
            margin_left='0',
            margin_right='0',
            margin_top='0',
            page_size='A4')
        path = '%s%s/certificado-%s.pdf' % (
            MEDIA_ROOT, 'certificado', certificado.pk)
        url = '%s%s/certificado-%s.pdf' % (
            MEDIA_URL, 'certificado', certificado.pk)
        if not os.path.isfile(path):
            fh = open(path, 'wb')
            fh.write(pdf)
            fh.close()
        return Response(url, status=status.HTTP_200_OK)

    error = {'error': 'Certificado não existe.'}
    return Response(error, status=status.HTTP_400_BAD_REQUEST)


class InscricaoView(APIView):
    queryset = Inscricao.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def post(self, request, format=None):
        serializer = InscricaoSerializer(data=request.data, context={
                                         'request': self.request})
        if serializer.is_valid():
            tipo = TipoInscricao.objects.get(pk=request.data['tipo'])
            participante = Participante.objects.get(pk=self.request.user.pk)
            serializer.save(participante=participante)

            ctx = {
                'edicao': '%s - %s' % (tipo.edicao.evento.nome, tipo.edicao.edicao)
            }

            message = get_template(
                'email/confirm_inscricao.html').render(Context(ctx))
            msg = EmailMessage(
                tipo.edicao.nome(),
                message,
                to=[participante.email],
                from_email='contato@rease.com.br')
            msg.content_subtype = 'html'
            msg.send()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):
        inscricoes = self.get_queryset()
        serializer = InscricaoSerializer(inscricoes, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        today = date.today()
        return Inscricao.objects.filter(
            participante=self.request.user, tipo__edicao__termino__gte=today)


class EdicaoView(ListAPIView):
    queryset = Edicao.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def get(self, request, format=None):
        serializer = EdicaoSerializer(self.get_queryset(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        today = date.today()
        user = self.request.user
        all_edicoes = Edicao.objects.filter(
            inicio_inscricao__lte=today,
            termino_inscricao__gte=today
        ).exclude(
            edicao_tipoinscricao__tipo_inscricao__participante__pk=user.pk)
        edicoes = []
        for edicao in all_edicoes:
            if edicao.has_vagas():
                edicoes.append(edicao)
        return edicoes


class CertificadoMinicursoView(APIView):
    queryset = CertificadoMinicurso.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def get(self, request, format=None):
        certificados = self.get_queryset()
        serializer = CertificadoMinicursoSerializer(certificados, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return CertificadoMinicurso.objects.filter(
            inscricao__participante=self.request.user,
            turma__minicurso__edicao__certificado=True)


class CertificadoView(APIView):
    queryset = Certificado.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def put(self, request, pk, format=None):
        certificado = Certificado.objects.get(pk=pk)
        certificado.nome_certificado = request.data['nome_certificado']
        certificado.save()
        return Response("Nome definido com sucesso.")


class CertificadoPalestraView(APIView):
    queryset = CertificadoPalestra.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def get(self, request, format=None):
        certificados = self.get_queryset()
        serializer = CertificadoPalestraSerializer(certificados, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return CertificadoPalestra.objects.filter(
            inscricao__participante=self.request.user,
            palestra__edicao__certificado=True)


class CertificadoAtividadeView(APIView):
    queryset = CertificadoAtividade.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def get(self, request, format=None):
        certificados = self.get_queryset()
        serializer = CertificadoAtividadeSerializer(certificados, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return CertificadoAtividade.objects.filter(
            inscricao__participante=self.request.user,
            turma__atividade__edicao__certificado=True)


class CertificadoEventoView(APIView):
    queryset = CertificadoEvento.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def get(self, request, format=None):
        certificados = self.get_queryset()
        serializer = CertificadoEventoSerializer(certificados, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        return CertificadoEvento.objects.filter(
            inscricao__participante=self.request.user,
            inscricao__tipo__edicao__certificado=True)


class MinicursoView(ListAPIView):
    queryset = Minicurso.objects.all()
    serializer_class = MinicursoSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']


class GrupoView(ListAPIView):
    queryset = Grupo.objects.all()
    serializer_class = GrupoSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']


class TurmaMinicursoView(ListAPIView):
    queryset = TurmaMinicurso.objects.all()
    serializer_class = TurmaMinicursoSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['minicurso', 'minicurso__edicao']


class PalestraView(ListAPIView):
    queryset = Palestra.objects.all()
    serializer_class = PalestraSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']


class AtividadeGeralView(ListAPIView):
    queryset = AtividadeGeral.objects.all()
    serializer_class = AtividadeGeralSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']


class AtividadeView(ListAPIView):
    queryset = Atividade.objects.all()
    serializer_class = AtividadeSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']


class TurmaAtividadeView(ListAPIView):
    queryset = TurmaAtividade.objects.all()
    serializer_class = TurmaAtividadeSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ('atividade', 'atividade__edicao')


class TipoInscricaoView(ListAPIView):
    queryset = TipoInscricao.objects.all()
    serializer_class = TipoInscricaoSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['edicao']


class TipoDocumentoView(ListAPIView):
    queryset = TipoDocumento.objects.all()
    serializer_class = TipoDocumentoSerializer
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['tipo_inscricao']


class DocumentoView(APIView):
    queryset = Documento.objects.none()
    permission_classes = [IsAuthenticated, IsParticipante]
    authentication_classes = [TokenAuthentication]

    def post(self, request, format=None):
        serializer = DocumentoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
