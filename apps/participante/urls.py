from django.urls import path
from . import views


app_name = 'participante'

urlpatterns = [
    path('certificados/nome/<int:pk>/', views.CertificadoView.as_view(), name='certificado-minicurso-nome'),
    path('inscricoes/', views.InscricaoView.as_view(), name='inscricao-api'),
    path('tipos/inscricao/', views.TipoInscricaoView.as_view(), name='tipo-inscricao-api'),
    path('edicoes/', views.EdicaoView.as_view(), name='edicoes-api'),
    path('minicursos/', views.MinicursoView.as_view(), name='minicurso-api'),
    path('palestras/', views.PalestraView.as_view(), name='palestra-api'),
    path('atividades/', views.AtividadeView.as_view(), name='atividade-api'),
    path('atividades/gerais/', views.AtividadeGeralView.as_view(), name='atividade-geral-api'),
    path('documentos/', views.DocumentoView.as_view(), name='documento-api'),
    path('grupos/', views.GrupoView.as_view(), name='grupo-api'),
    path('turmas/minicurso/', views.TurmaMinicursoView.as_view(),
        name='turma-minicurso-api'),
    path('turmas/atividade/', views.TurmaAtividadeView.as_view(),
        name='turma-atividade-api'),
    path('certificados/minicurso/', views.CertificadoMinicursoView.as_view(),
        name='certificado-minicurso-api'),
    path('certificados/palestra/', views.CertificadoPalestraView.as_view(),
        name='certificado-palestra-api'),
    path('certificados/evento/', views.CertificadoEventoView.as_view(),
        name='certificado-evento-api'),
    path('certificados/atividade/', views.CertificadoAtividadeView.as_view(),
        name='certificado-atividade-api'),
    path('certificados/<str:token>)/',
        views.generate_certificado, name='generate-certificado')
]
