from django.apps import AppConfig


class ParticipanteConfig(AppConfig):
  name = 'apps.participante'
