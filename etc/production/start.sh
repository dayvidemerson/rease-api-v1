#!/bin/bash
set -e
LOGFILE=/root/django/rease-v1/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=1
USER=root
ADDRESS=127.0.0.1:8000
source /root/django/rease-v1/env/bin/activate
cd /root/django/rease-v1/project/
test -d $LOGDIR || mkdir -p $LOGDIR
exec gunicorn -w $NUM_WORKERS --bind=$ADDRESS --user=$USER --log-level=debug --log-file=$LOGFILE rease.wsgi:application
