# Rease - Sistema de Eventos
## API - REST
### MIT - License

Requisitos:
- Python 3.X
- Django 2.X
- Postgres 10.X

Como configurar:
1. Criar banco de dados com o nome que desejar
2. Criar copia do arquivo `example.env`
3. Renomear copia para `.env`
4. Alterar arquivo `.env` com as configurações do dispositivo onde o sistema será executado
5. (Opcional) Criar virtualenv, preferencialmente com o nome `venv` ou `env` e em seguida inicialize a virtualenv
6. Execute o comando `make pip` ou `pip install -r requirements.txt`
7. Execute o comando `make migrate` ou `python manage.py migrate`
8. Execute o comando `make server` ou `python manage.py runserver`
9. Acesse `http://localhost:8000/docs` e você estará acessando a documentação da api de forma interativa para testes.

Desenvolvedor: `Dayvid Emerson` <dayvidemerson96@gmail.com>
