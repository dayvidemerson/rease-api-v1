from django.conf import settings
from django.urls import path, include
from django.contrib import admin

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.documentation import include_docs_urls

from .settings import MEDIA_ROOT


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-token-auth/', obtain_auth_token),

    path('core/', include('apps.core.urls')),
    path('evento/', include('apps.evento.urls')),
    path('administrador/', include('apps.administrador.urls')),
    path('participante/', include('apps.participante.urls')),
    
    path('docs', include_docs_urls(title='Documentação da API'))
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)